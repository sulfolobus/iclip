Purpose
=======

The collection of scripts at hand is provided for the evaluation of
the very specific iCLIP sequencing data of *Sulfolobus solfataricus*.
This is not meant to be a general purpose iCLIP data evaluation pipeline,
even though you might inspect the steps performed to get an idea on
how to evaluate your own iCLIP data. But be aware that the code has
not been developed for optimal performance nor for top code quality.

The scripts provided primarily serve documentation purposes, to
achieve high TRANSPARENCY and REPRODUCIBILITY of the data evaluation
for the specific sequencing data.


Citation
========

If you find the scripts within this repository to be useful in you
personal project, please cite the following publication:

> Jochen Bathke, Susann Gauernack, Oliver Rupp, Lennart Weber, Christian Preusser, Marcus Lechner, Oliver Rossbach, Alexander Goesmann, Elena Evguenieva-Hackenberg, Gabriele Klug<br>
> iCLIP analysis of RNA substrates of the archaeal exosome<br>
> BMC Genomics 21, 797 (2020). https://doi.org/10.1186/s12864-020-07200-x<br>


System requirements
=====================

Some of the substeps are computationally more demanding than others.
This is especially true for some blastn searches, that would require
several hours of computing time when performed single threaded (at
time of writing in 2018 on standard x64 CPUs). Therefore several steps
rely on simultaneous threads (up to 20 parallel blastn searches).

The minimum system requirements for the scripts to be executed without
modification are therefore:
* 20 cores
* 128 GB main memory
* 60 GB free disk space

The time required for the scripts to finish is approx.
	190 min
when being executed on the following hardware:
* 64 threads (Intel(R) Xeon(R) CPU E7-4830 @ 2.13GHz)
* 512 GB main memory


Usage
=====

Data evaluation is started by invoking the start script:
```
./run_all.sh
```

If the script is not executable invoke ``chmod u+x run_all.sh`` first.

The scripts requires the input data to be place
at a specific place (relative to the script location):
  ```
  ../unzipped_Data_and_fastqc/Exo_S1_L001_R1_001.fastq
  ```
Also the output will be written to a specific place, which might be
altered by changing a variable in ``run_all.sh``:
  ```
  results_dir=../Sulfolobus_data_evalutation_dir
  ```


Program versions
=================

The original data evaluation has been performed under the following
operating system (``uname -a``):
  ```
  Linux 4.15.0-32-generic #35~16.04.1-Ubuntu SMP Fri Aug 10\
  21:54:34 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
  ```

The exact program versions used are listed at the beginning of the
respective shell scripts.
