#!/bin/bash

# Start the evaluation pipeline with all steps.

###################################################
: '
Programm versions used during analysis:
 GNU bash, version 4.3.48(1)-release
'
###################################################

# write the results to the following directory
results_dir=../Sulfolobus_data_evalutation_dir

# path pointing to the input data
input_data="../unzipped_Data_and_fastqc/Exo_S1_L001_R1_001.fastq"

# create the top level results_directory
mkdir $results_dir

echo "
***************************************************
	Part I - read processing & mapping
***************************************************

"

first_step_dir=01_read_processing
./scripts_bash/read_processing.sh $results_dir/$first_step_dir $input_data 

echo "
***************************************************
        Part II - find crosslink sites
***************************************************

"

second_step_dir=02_x-link_detection
./scripts_bash/x-link_search.sh $results_dir/$second_step_dir $results_dir/$first_step_dir/10_split_strands/BC*bam

echo "
***************************************************
        Part III - detect circular RNAs
***************************************************

"

third_step_dir=03_circRNA_detection
./scripts_bash/circRNA_detection.sh $results_dir/$third_step_dir $results_dir/$first_step_dir/09_mapping/BC*_unaligned.fasta

echo "
***************************************************
        Part VI - analyze 3' overhangs
***************************************************

"
fourth_step_dir=04_3P_overhangs
./scripts_bash/analyze_3P_overhangs.sh $results_dir/$fourth_step_dir $results_dir/$first_step_dir/09_mapping/BC*_unaligned.fasta

echo "
***************************************************
	FINISHED
***************************************************
"
