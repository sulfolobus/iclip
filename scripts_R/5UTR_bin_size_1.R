#!/usr/bin/env Rscript

args = commandArgs(trailingOnly=TRUE)
print(args)

file_rrp4 = args[1]
file_rrp41 = args[2]
dir_name = dirname(file_rrp4)

print(file_rrp4)
print(file_rrp41)
print(dir_name)

############################
# Factors for the positioning
# of graphical elements
Y.LEG.POS=0.95
Y.XLAB.POS=-0.11
Y.WRIT.POS=0.04
Y.BUFFER=1.02

############################
# Generalized function for
# the generation of the
# final graphic

DistributionPlot = function(values.4, values.41,
                           myLabel=c("Crosslink distribution around the 5'-end of genes",
                                     "Crosslink count",
                                     "5'-UTR (distances in nucleotides)",
                                     "5'-end of annotated genes",
                                     "beginning of putative 5'-UTR")){
  (y.max = max(c(values.4, values.41)))
  y.legend = y.max * Y.LEG.POS
  y.xlab = y.max * Y.XLAB.POS
  y.writing = y.max * Y.WRIT.POS
  y.max = y.max * Y.BUFFER
  
  # xaxt="n" prevents labels and tickmarks on the x-axis, y-axis as usual
  plot(x=c(seq(1,200)), y=values.4, type="n", xaxt="n", xlab="", ylab="", ylim=c(0,y.max))
  # draw tickmarks on the x-axis
  axis(side=1, at=c(seq(1,100,10),seq(110,200,10)), labels=F)
  label.list=c(seq(-100,-1,10),seq(10,100,10))
  # add labels to the tickmarks
  text(x=c(seq(1,100,10),seq(110,200,10)), y=y.xlab, labels=label.list, srt=45, xpd=TRUE, adj=1)
  mtext(side=1, text=myLabel[3], line=3.5)
  mtext(side=2, text=myLabel[2], line=2.5)
  # draw the line itself
  lines(x=seq(200), y=(values.4), type="o", pch=21, col="black", bg="white")
  lines(x=seq(200), y=(values.41), type="o", pch=21, col="black", bg="black", lty=2)
  
  legend(x=1, y=y.legend, legend=c("Rrp4", "Rrp41"), pch=c(21,21), pt.bg=c( "white", "black"), lty=c(1,2))
  
  title(main=myLabel[1])
  
  # add vertical line to indicate ending and beginning
  abline(v=100.5, lty=2, col="gray")
  text(100.5, y.writing, myLabel[5], pos=4, col="grey")
  text(100.5, y.writing, myLabel[4], pos=2, col="grey")
}

calc.percentage = function(x, max.value){
  final.result = x * 100 / max.value
  return(final.result)
}

############################
# Graphic for genes
# absolute numbers

rrp4.genes_1 = scan(file=file_rrp4, what=list("",0), sep="\t")
rrp41.genes_1 = scan(file=file_rrp41, what=list("",0), sep="\t")

png(paste(dir_name, "/absolut_distribution_of_cross-links.png", sep=""), width=2100, height=1500, units="px", res=300)
DistributionPlot(rrp4.genes_1[[2]], rrp41.genes_1[[2]], c("Crosslink distribution around the 5'-beginning of genes",
                                                         "Crosslink count",
                                                         "5'-UTR (distances in nucleotides)",
                                                         "end of putative 5'-UTR",
                                                         "5'-beginning of annotated genes"))
dev.off()

############################
# Graphic for genes
# relative numbers


rrp4.genes_1 = scan(file=file_rrp4, what=list("",0), sep="\t")
rrp41.genes_1 = scan(file=file_rrp41, what=list("",0), sep="\t")
# calculate relative numbers
rrp4.genes_1[[2]] = calc.percentage(rrp4.genes_1[[2]], sum(rrp4.genes_1[[2]]))
rrp41.genes_1[[2]] = calc.percentage(rrp41.genes_1[[2]], sum(rrp41.genes_1[[2]]))

png(paste(dir_name, "/relative_distribution_of_cross-links.png", sep=""), width=2100, height=1500, units="px", res=300)
DistributionPlot(rrp4.genes_1[[2]], rrp41.genes_1[[2]], c("Crosslink distribution around the 5'-beginning of genes",
                                                         "Relative crosslink count (%)",
                                                         "5'-UTR (distances in nucleotides)",
                                                         "end of putative 5'-UTR",
                                                         "5'-beginning of annotated genes"))
dev.off()
