#!/bin/bash

# Uses the mapped reads - in sam format - to create an new Wig file,
# containing only the positions of the putative x-link positions.
#
# Usage:
# ./create_wig.sh input-file.sam out-file.wig

inFile=${1:?undefined input file}
newFile=${2:?undefined ouput file}

echo 'track type=wiggle_0 name="XY" description="XY"' > $newFile
echo 'variableStep chrom=Chromosome span=1' >> $newFile

tail -n+4 $inFile | cut -f4 | sort -n | uniq -c |\
while read var2 var1
do
	echo $(( ${var1:?undefiniert} - 1 )) ${var2:?undefiniert};
done >> $newFile
