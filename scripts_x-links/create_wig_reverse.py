#!/usr/bin/env python3

import sys
import re

DEBUG = True

dateiName = sys.argv[1]
datei = open(sys.argv[1], "r")

# function for the parsing of the CIGAR strings:
"""
    1. identity  := M
    2. deletion  := D (missing in Query sequence), only given as -)
    3. insertion := I (missing in Sbjct sequence, only given as -)
    4. mutation  := X
    [MIDNSHPX=]
    have a look at documenation  SAM format: table page 5 
"""

positionDictionary = dict()

def parseCIGAR(cigarString):
    count = 0
    tokens = ""
    # split the CIGAR string into single tokens
    for char in cigarString:
        count += 1
        if( char == 'M' or char == 'I' or char == 'D' or char == 'X' or
            char == 'N' or char == 'S' or char == 'H' or char == 'P' or
            char == '=' ):
            #print("hit:  ", cigarString[:count])
            #print("rest: ", cigarString[count:])
            tokens += (" " + cigarString[:count])
            cigarString = cigarString[count:]
            count = 0
    tokens = tokens.lstrip(" ").split(" ")
    summe = 0
    # create the sum of the relevant tokens
    for token in tokens:
        if( token[-1] == 'M' or token[-1] == 'D' or token[-1] == 'X' or
            token[-1] == 'N' or token[-1] == '=' ):
             summe += int( token[:-1] )
    return summe


for line in datei:
    if line[0] == "@":
        continue
    lineArray = line.rstrip().split("\t")
    endPosition = int(lineArray[3])
    cigarString = lineArray[5]

    laenge = parseCIGAR(cigarString)

    
    startPosition = endPosition + laenge

    if startPosition not in positionDictionary:
        positionDictionary[startPosition] = 1
    else:
        positionDictionary[startPosition] += 1

# print the header of the Wig file:
print('track type=wiggle_0 name="', dateiName.split(".")[0], '" description="x-link sites"', sep="")
print('variableStep chrom=Chromosome span=1')

keyList = sorted(positionDictionary.keys())
for key in keyList:
    print(key, positionDictionary[key], sep=" ")
