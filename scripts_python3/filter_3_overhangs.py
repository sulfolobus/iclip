#!/usr/bin/env python3

# Objectives:
# Determine the 3' overhangs within those reads resulting in blast hits.
# Three input files are needed:
# 1. fasta file containing the inital sequences
# 2. results from the last blastn search (tsv file)
# 3. list of the 5' fragments

DEBUG = False #True

import sys

# fasta file with all 3' attachments
fastaFile = open(sys.argv[1], "r")

# tsv file originating from blastn with all hits containing a 3' attachment
blastResults_1 = open(sys.argv[2], "r")

# tsv file of the 2nd blastn run, resulting in long 3' hits against the genome
blastResults_2 = open(sys.argv[3], "r")

fastaDict = dict()
blastDict = dict()

# read fasta file
# fastaID = key, fasta sequence = value
for line in fastaFile:
    line = line.rstrip()
    wert = next(fastaFile).rstrip()
    fastaDict[line] = wert

# read blast file
# fastaID = key, remaining line = value
for line in blastResults_1:
    line = line.rstrip()
    key = line.split("\t")[0]
    wert = line#.split("\t")[1:]
    blastDict[key] = wert

if DEBUG: print("Length fasta dict:", len(fastaDict))
if DEBUG: print("Length blast dict:", len(blastDict))

# remove the hits from the two dictonaries
count=0
for line in blastResults_2:
    fastaID = line.rstrip().split("\t")[0]
    del fastaDict[">" + fastaID]
    del blastDict[fastaID]

if DEBUG: print("Length fasta dict:", len(fastaDict))
if DEBUG: print("Length blast dict:", len(blastDict))

# output the two processed dictonaries
fileOut = open(sys.argv[4], "w")
for key in fastaDict:
    #print(key)
    #print(fastaDict[key])
    fileOut.write(key + "\n")
    fileOut.write(fastaDict[key] + "\n")
fileOut.close()

fileOut = open(sys.argv[5], "w")
for key in blastDict:
    #print(blastDict[key])
    fileOut.write(blastDict[key] + "\n")
fileOut.close()
