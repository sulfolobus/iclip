#!/usr/bin/env python3
import sys

if len(sys.argv) != 4:
    print("Wrong  usage!\n5UTR_bins.py annotation.gff bins_size number_of_bins")
    sys.exit()

annotation = open(sys.argv[1], "r")
binSize = int(sys.argv[2])
binNumber = int(sys.argv[3])

for line in annotation:
    line = line.rstrip()
    lineSplit = line.split()
    strand = lineSplit[6]
    if strand == "+":
        start = int(lineSplit[3])
	# 10-sized bins before the beginning (before the operon)
        for i in reversed(range(1,binNumber+1)):
           binStart = start - i*binSize
           binEnd = start - (i-1)*binSize -1
           print(lineSplit[0], lineSplit[1], lineSplit[2], binStart, binEnd, ".", strand, ".", lineSplit[-1] + "_bin_-" + str(i), sep="\t")
        print(line)
	# 10-sized bins after the beginning (within the operon)
        for i in range(1,binNumber+1):
           binStart = start + (i-1)*binSize
           binEnd = start + i*binSize - 1
           print(lineSplit[0], lineSplit[1], lineSplit[2], binStart, binEnd, ".", strand, ".", lineSplit[-1] + "_bin_+" + str(i), sep="\t")
    if strand == "-":
        start = int(lineSplit[4])
	# 10-sized bins before the beginning (before the operon)
        for i in reversed(range(1,binNumber+1)):
           binStart = start + (i-1)*binSize + 1
           binEnd = start + i*binSize
           print(lineSplit[0], lineSplit[1], lineSplit[2], binStart, binEnd, ".", strand, ".", lineSplit[-1] + "_bin_-" + str(i), sep="\t")
        print(line)
	# 10-sized bins after the beginning (within the operon)
        for i in range(1,binNumber+1):
           binStart = start - i*binSize + 1
           binEnd = start - (i-1)*binSize
           print(lineSplit[0], lineSplit[1], lineSplit[2], binStart, binEnd, ".", strand, ".", lineSplit[-1] + "_bin_+" + str(i),     sep="\t")
