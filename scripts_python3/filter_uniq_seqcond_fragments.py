#!/usr/bin/env python3

# Usage:
# ./filter_uniq_seqcond_fragments.py 2nd_blastn_minus_strand_filtered_1.tsv
# Uniq hits: printed to stdout
# Ambiguous hits: printed to stderr

import sys
from collections import defaultdict

# Have enough arguments been passed?
if len(sys.argv) != 2:
    print("Wrong usage!\nfilter_uniq_seqcond_fragments.py 2nd_blastn_minus_strand_filtered_1.tsv\nExit.")
    sys.exit()

second_matches = sys.argv[1]
file_second_matches = open(second_matches, "r")

multimap = defaultdict(list)

# read everything into the MultiMap
for line in file_second_matches:
    line = line.rstrip()
    ID = line.split("\t")[0]
    multimap[ID].append(line)

# separate unique and ambiguous hits:
for key in multimap:
    # output unique hits
    if len(multimap[key]) == 1:
        print(multimap[key][0])
    # ouput all ambiguous hits:
    else:
        for item in multimap[key]:
            print(item, file=sys.stderr)
