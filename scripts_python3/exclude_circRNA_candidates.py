#!/usr/bin/env python3

import sys

# Objectives:
# Determine the 3' attachments within those reads that generated blast hits.
# Three input files are needed:
# 1. fasta file containing the source sequences
# 2. the results of the blastn search (as tsv file)
# 3. the list of the 5' fragments

DEBUG = False#True

#fastaFile = open("3Poverhangs_filtered.fasta","r")
fastaFile = open(sys.argv[1], "r")

#blastResults_1 = open("filteredMappingTest_filtered.tsv", "r")
blastResults_1 = open(sys.argv[2], "r")

# second blastn results
blastResults_2 = open(sys.argv[3], "r")

fastaDict = dict()
blastDict = dict()

# read fasta file
# fastaID = key, fasta sequence = value
for line in fastaFile:
    line = line.rstrip()
    wert = next(fastaFile).rstrip()
    fastaDict[line] = wert

# read blast file
# fastaID = key, remeining line = value
for line in blastResults_1:
    line = line.rstrip()
    key = line.split("\t")[0]
    wert = line.split("\t")[1:]
    blastDict[key] = wert

if DEBUG: print("Length fasta dict:", len(fastaDict), file=sys.stderr)
if DEBUG: print("Length blast dict:", len(blastDict), file=sys.stderr)

# filter the 3' attachments for potential circRNAs
for line in blastResults_2:
    line = line.rstrip()
    lineArray = line.split("\t")
    fastaID = lineArray[0]
    # test the preceeding 4000 bp
    # remove 3' attachments if potential circRNA candidate
    position1stHit = int(blastDict[fastaID][3]) # positon of the 2ter blastn hit
    position2ndHit = int(lineArray[5])
    strand1stHit = blastDict[fastaID][-1]
    strand2ndHit = lineArray[-1]
    if strand1stHit != strand2ndHit:
        continue
    if strand1stHit == "plus":
        if position1stHit-4000 < position2ndHit < position1stHit:
            try:
                del fastaDict[">" + fastaID]
                if DEBUG: print("Entferne read:", fastaID, "\t\tauf plus-Strang:", strand1stHit, file=sys.stderr)
            except:
                pass
    if strand1stHit == "minus":
        if position1stHit < position2ndHit < position1stHit+4000:
            try:
                del fastaDict[">" + fastaID]
                if DEBUG: print("Entferne read:", fastaID, "\t\tauf minus-Strang:", strand1stHit, file=sys.stderr)
            except:
                pass

# output the processd dictonary of the 3' attachments
for key in fastaDict:
    print(key)
    print(fastaDict[key])
