#!/usr/bin/env python3

# Script is adapted for the minus-strand, as the coordinates have to be treated in an inverted way.
# Usage:
# select_second_fragments.py 1st_blastn_minus_overhang.tsv 2nd_blastn_minus_strand.tsv > ausgabe.tsv

import sys

# maximum distance for the ends of a circRNA
MAX_DISTANCE = 4000
# minmum distance for the ends of a circRNA
# the minimum length of the better hit have at least to be within 20 nt
# the circRNA to be found cannot be shorter
# this also filters sequenceing artifacts, as the revers transcriptas can duplicate parts of the sequence
MIN_DISTANCE = 20

# Have enough arguments been passed?
if len(sys.argv) != 3:
    print("Wrong usage!\nselect_second_fragments.py 1st_blastn_minus_overhang.tsv 2nd_blastn_minus_strand.tsv > ausgabe.tsv\nExit.")
    sys.exit()

first_matches = sys.argv[1]
file_first_matches = open(first_matches, "r")

second_matches = sys.argv[2]
file_second_matches = open(second_matches, "r")

# Object to store teh data of the blastn results
class blastn_match:
        def __init__(self, qseqid=None, qlen=None, length=None, qstart=None, qend=None, sstart=None, send=None,\
                     sstrand=None, qseq=None, sseq=None, evalue=None, pident=None, nident=None, mismatch=None):
            self.qseqid = qseqid
            self.qlen = qlen
            self.length = length
            self.qstart = qstart
            self.qend = qend
            self.sstart = sstart
            self.send = send
            self.sstrand =sstrand
            self.qseq = qseq
            self.sseq = sseq
            self.evalue = evalue
            self.pident = pident
            self.nident = nident
            self.mismatch = mismatch

# function to create an object
def createMatch(blastn_result):
    lineArray = blastn_result.rstrip().split("\t")
    match = blastn_match( qseqid = lineArray[0],\
                          qlen = int( lineArray[1] ),\
                          length = int( lineArray[2] ),\
                          qstart = int( lineArray[3] ),\
                          qend = int( lineArray[4] ),\
                          sstart = int( lineArray[5] ),\
                          send = int( lineArray[6] ),\
                          sstrand = lineArray[7],\
                          qseq = lineArray[8],\
                          sseq = lineArray[9],\
                          evalue = lineArray[10],\
                          pident = lineArray[11],\
                          nident = lineArray[12],\
                          mismatch = int( lineArray[13] )\
                        )
    return match

# dictonary containing all blastn results
# IDs = key; rest will be stored in an object
firstHitsDict = dict()
for line in file_first_matches:
    match = createMatch(line)
    firstHitsDict[match.qseqid] = match

# iterate over the second hits and filter:
for line in file_second_matches:
    second_match = createMatch(line)
    fastaID = second_match.qseqid.split("_")[0]
    # is the second hit upstream oder downstream wihtin the read?
    if "upstream" in second_match.qseqid:
        upstreamEnd = second_match.send
        downstreamStart = firstHitsDict[fastaID].sstart
        # are the distances ok?
        if (downstreamStart - MAX_DISTANCE) < upstreamEnd < (downstreamStart - MIN_DISTANCE):
            # does the upstream hit align at its 3' end?
            # there shouldn't be any gap to the following fragment
            if second_match.qend == second_match.qlen:
                print(line, end="", sep="")# can be readout again
    if "downstream" in second_match.qseqid:
        pass
        upstreamEnd = firstHitsDict[fastaID].send
        downstreamStart = second_match.sstart
        # are the distances ok?
        if (downstreamStart - MAX_DISTANCE) < upstreamEnd < (downstreamStart - MIN_DISTANCE):
            # Does the downstream hit start directly with the first base?
            # There shouldn't be any gap to the following fragment.
            if second_match.qstart == 1:
                print(line, end="", sep="")# can be readout again

