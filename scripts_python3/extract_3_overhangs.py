#!/usr/bin/env python3

# Objectives:
# Determine the 3' attachments within those reads that gave rize to blast hits.
# Two input files are needed:
# 1. fasta file containing the original sequences
# 3. the results of the blastn search (as tsv file)

import sys

#fastaFile = open("../0_unmapped_reads/forward_BC1_unaligned.fasta","r")
fastaFile = open(sys.argv[1], "r")

#blastResults = open("filteredMappingTest.tsv", "r")
blastResults = open(sys.argv[2], "r")

fastaDict = dict()

# read the fasta file
# fastaID = key; fasta sequence = value
for line in fastaFile:
    line = line.rstrip()
    wert = next(fastaFile).rstrip()
    fastaDict[line] = wert

for line in blastResults:
    lineArray = line.rstrip().split("\t")
    seqID = lineArray[0]
    qend = int(lineArray[2])
    sequence = fastaDict[">" + seqID]
    # output the fastaID & sequence of the attachment
    print(">", seqID, sep="")
    print(sequence[qend:])
