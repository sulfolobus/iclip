#!/usr/bin/env python3
import sys

DEBUG = False
DEBUG2 = False

if len(sys.argv) != 2:
    print("Wrong usage!\ndecile.py in_file")
    sys.exit()

inFile = open(sys.argv[1], "r")

def createOutput(start, end, line, position):
    print(line.split()[0], line.split()[1], line.split()[2], start, end,
          line.split()[5], line.split()[6], line.split()[7], line.split()[8] + ":" + position, sep="\t")

for line in inFile:
    line = line.rstrip()
    
    if line == "###":
        print(line)
        continue
    else:
        print(line + ":full_gene") # also output the old line
    
    # now split the line and write a new entry for every third
    # remark about the gff3 format:
    # start & stop position both include the respective base position
    geneStart = int(line.split()[3])
    geneEnd = int(line.split()[4])
    geneLength = geneEnd - geneStart + 1 # +1 as the last base is inluded
    # Upon which strand is the respective gene or feature located?
    strand = line.split()[6]
    # split every gene into deciles
    decil = geneLength / 10.0
    if DEBUG:
      print("deczil length:", decil)
      print("Beginning:    ", geneStart)
      print("End:          ", geneEnd)
      print("Length:       ", geneLength)
    
    # an output for every decile:
    for i in range(10):
        decilStart = geneStart + round(decil * i)
        decilEnd = geneStart + round(decil * (i+1)) - 1 # -1 as the last base is included
        if DEBUG:
          print("Decil", i, "Beginning:", decilStart)
          print("Decil", i, "End:      ", decilEnd)
        if strand == "+":
            createOutput(decilStart, decilEnd, line, "Decil_" + str(i+1))
        else: # has to be located on the (-) strand => the deciles have to be reversed
            createOutput(decilStart, decilEnd, line, "Decil_" + str(10-i))
