#!/usr/bin/env python3

import sys

fastaFile = open(sys.argv[1], "r")

maxLength = 0
for line in fastaFile:
    line = line.rstrip()
    if maxLength < len(line):
        maxLength = len(line)

countA = [0 for x in range(maxLength)]
countT = [0 for x in range(maxLength)]
countC = [0 for x in range(maxLength)]
countG = [0 for x in range(maxLength)]
countElse = [0 for x in range(maxLength)] # shouldn't be possible in the given data set

fastaFile.seek(0)
for line in fastaFile:
    line = line.rstrip()
    # only look at the lines containing sequences, don't look at the fastaIDs
    if line[0] != ">":
        lineLength = len(line)
#        if lineLength >= 100:
#            print("Sequenz zu lang! Ende")
#            break
        for i in range(lineLength):
            if line[i] == "A":
                countA[i] += 1
            elif line[i] == "T":
                countT[i] += 1
            elif line[i] == "C":
                countC[i] += 1
            elif line[i] == "G":
                countG[i] += 1
            else:
                countElse[i] += 1

def myOutput(liste):
    for i in range(maxLength):
        print(liste[i], end="\t") 
    print()

print(" \t", end="")
for i in range(maxLength):
    print(i+1, end="\t")
print()

print("A:\t", end="")
myOutput(countA)
print("T:\t", end="")
myOutput(countT)
print("C:\t", end="")
myOutput(countC)
print("G:\t", end="")
myOutput(countG)
print("?:\t", end="")
myOutput(countElse)
