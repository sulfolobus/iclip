#!/usr/bin/env python3

# Usage:
# filter_2nd_fragment.py 1st_blastn_matches.tsv input_reads.fasta > output.fasta

import sys

# Have enough arguments been passed?
if len(sys.argv) != 3:
    print("Wrong usage!\nfilter_2nd_fragment.py 1st_blastn_matches.tsv input_reads.fasta > output.fasta\nExit.")
    sys.exit()

first_matches = sys.argv[1]
file_matches = open(first_matches, "r")

primary_reads = sys.argv[2]
file_reads = open(primary_reads, "r")

# object to store the data of the blastn results
class blastn_match:
    def __init__(self, qseqid=None, qlen=None, length=None, qstart=None, qend=None, sstart=None, send=None,\
                 sstrand=None, qseq=None, sseq=None, evalue=None, pident=None, nident=None, mismatch=None):
        self.qseqid = qseqid
        self.qlen = qlen
        self.length = length
        self.qstart = qstart
        self.qend = qend
        self.sstart = sstart
        self.send = send
        self.sstrand =sstrand
        self.qseq = qseq
        self.sseq = sseq
        self.evalue = evalue
        self.pident = pident
        self.nident = nident
        self.mismatch = mismatch

# for debugging: output the object
def printMatch(match):
    print("qseqid:", match.qseqid, "\nqlen:", match.qlen, "\nlength:", match.length, "\nqstart:", match.qstart, "\nqend:", match.qend,\
          "\nsstart:", match.sstart, "\nsend:", match.send, "\nsstrand:", match.sstrand, "\nqseq:", match.qseq, "\nsseq:", match.sseq, \
          "\nevalue:", match.evalue, "\npident:", match.pident, "\nnident:", match.nident, "\nmismatch:", match.mismatch)

# dictonary of all blastn results
# ID = key, rest will be stored as an object (used as data structure)
blastnDict = dict()
for line in file_matches:
    lineArray = line.rstrip().split("\t")
    match = blastn_match( qseqid = lineArray[0],\
                          qlen = int( lineArray[1] ),\
                          length = int( lineArray[2] ),\
                          qstart = int( lineArray[3] ),\
                          qend = int( lineArray[4] ),\
                          sstart = int( lineArray[5] ),\
                          send = int( lineArray[6] ),\
                          sstrand = lineArray[7],\
                          qseq = lineArray[8],\
                          sseq = lineArray[9],\
                          evalue = lineArray[10],\
                          pident = lineArray[11],\
                          nident = lineArray[12],\
                          mismatch = int( lineArray[13] )\
                        )

    blastnDict[match.qseqid] = match

# Debugging:
#    print(line)
#    printMatch( blastnDict[match.qseqid] )
#    break

# create dictonary for all reads
# ID = key (without >)
readDict = dict()
for line in file_reads:
    ID = line.rstrip()[1:] # ohne > einlesen
    seq = file_reads.readline().rstrip()
    readDict[ID] = seq

# zum Debuggen:
#counter = 0
#for key in readDict:
#    print(key, "\n", readDict[key])
#    counter += 1
#    if counter > 10: break

# write attachments for all blastn hits into the object blastnDict
for key in blastnDict:
    fullSequence = readDict[key]
    match = blastnDict[key]
    # left sided attachment (upstream) with at least 12 bp
    # qstart > 12 needed
    if match.qstart > 12:
        print(">", key, "_upstream_fragment", sep="")
        print(fullSequence[:(match.qstart-1)], sep="")
        # for Debuggen:
#        print(" " * len(fullSequence[:(match.qstart-1)]), match.qseq, sep="")
#        print(fullSequence)
    # right sided attachment (downstream) with at least 12 bp
    # qlen - qend > 12
    if (match.qlen - match.qend) > 12:
        print(">", key, "_downstream_fragment", sep="")
        print(fullSequence[match.qend:])
        # for Debuggen:
#        print(match.qseq)
#        print(" " * len(match.qseq), fullSequence[match.qend:], sep="")
#        print(fullSequence)
