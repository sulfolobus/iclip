#!/usr/bin/env python3
import sys

if len(sys.argv) != 4:
    print("Wrong usage!\n3UTR_bins.py annotation.gff bins_size number_of_bins")
    sys.exit()

annotation = open(sys.argv[1], "r")
binSize = int(sys.argv[2])
binNumber = int(sys.argv[3])

for line in annotation:
    line = line.rstrip()
    lineSplit = line.split()
    strang = lineSplit[6]
    if strang == "+":
        ending = int(lineSplit[4])
	# 10-sized bins before the ending
        for i in reversed(range(1,binNumber+1)):
           binEnd = ending - (binSize*i) + 1
           binStart = ending - (binSize*(i-1))
           print(lineSplit[0], lineSplit[1], lineSplit[2], binEnd, binStart, ".", strang, ".", lineSplit[-1] + "_bin_-" + str(i), sep="\t")
        print(line)
	# 10-sized bins after the ending
        for i in range(1,binNumber+1):
           binStart = ending + (binSize*(i-1)) + 1
           binEnd = ending + (binSize*i)
           print(lineSplit[0], lineSplit[1], lineSplit[2], binStart, binEnd, ".", strang, ".", lineSplit[-1] + "_bin_+" + str(i), sep="\t")
    if strang == "-":
        ending = int(lineSplit[3])
	# 10-sized bins outside of the operons
        for i in reversed(range(1,binNumber+1)):
           binEnd = ending - i*binSize
           binStart = ending - (i-1)*binSize - 1
           print(lineSplit[0], lineSplit[1], lineSplit[2], binEnd, binStart, ".", strang, ".", lineSplit[-1] + "_bin_+" + str(i), sep="\t")
        print(line)
	# 10-sized bins within the operon
        for i in range(1,binNumber+1):
           binStart = ending + i*binSize - 1
           binEnd = ending + (i-1)*binSize
           print(lineSplit[0], lineSplit[1], lineSplit[2], binEnd, binStart, ".", strang, ".", lineSplit[-1] + "_bin_-" + str(i), sep="\t")
