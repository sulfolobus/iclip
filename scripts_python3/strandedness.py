#!/usr/bin/env python3
import sys

if len(sys.argv) != 2:
    print("Wrong usage!\nstrandedness.py in_file")
    sys.exit()

inFileDir = sys.argv[1]


gffFile = open(inFileDir+"/nurRefSeqChromosome.gff", "r")
bedFile = open(inFileDir+"/RefSeqChromosomeMerged.bed", "r")

gffDict = dict()

for line in gffFile:
  if line[0] != "#":
    line = line.rstrip()
    lineSplit = line.split()
    startPos = int(lineSplit[3])
    gffDict[startPos] = lineSplit

counter = 0

for line in bedFile:
    line = line.rstrip()
    lineSplit = line.split()
    # to gff coordinates
    startPos = int(lineSplit[1]) + 1
    endPos = int(lineSplit[2])
    # output in gff format
    counter += 1
    print(lineSplit[0], "me", "operon", startPos, endPos, ".", gffDict[startPos][6], ".", "ID=operon_" + str(counter), sep="\t")
