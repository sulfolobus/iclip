#!/usr/bin/env python3

# Usage: ./write_ligation_sites.py 2nd_blastn_minus_strand_uniq.tsv 1st_blastn_minus_overhang.tsv BC1_collapsed_min_32.fasta

import sys
from collections import defaultdict

# Have enough arguments been passed?
if len(sys.argv) != 4:
    print("Wrong usage!\n/write_ligation_sites.py 2nd_blastn_minus_strand_uniq.tsv 1st_blastn_minus_overhang.tsv BC1_collapsed_min_32.fasta\nExit.")
    sys.exit()

second_matches = sys.argv[1]
file_second_matches = open(second_matches, "r")
first_matches = sys.argv[2]
file_first_matches = open(first_matches, "r")
reads = sys.argv[3]
file_reads = open(reads, "r")

def createCigarString(query, subject):
    ''' 
    Opportunities for an alignment to occur:

    Query  1       TTACAAACT-CCCCGTCAATTTTCCATAGGGGTAGGAGGTCCATAAA  46
                   ||||||||| ||||||||| ||||||||||||||||| |||||||||
    Sbjct  873172  TTACAAACTCCCCCGTCAA-TTTCCATAGGGGTAGGAAGTCCATAAA  873217

    1. Identity  := M
    2. Deletion  := D (missing within the query sequence), which is shown as -)
    3. Insertion := I (missing within Sbjct sequence, which is shown as -)
    4. Mutation  := X

    In the best case both sequences are perfectly identical.
    '''

    # in case of perfect identity:
    if query == subject:
        return str(len(query)) + "M" 

    # if the sequences differ we need to create a CIGAR string
    # we need to iterate over the full sequence of the query and subject
    current = None
    previous = None
    cigarString = ""
    count = 0
    ic = 0 # ic := iteration counter
    for base in query:
        # compare query and subject
        if base == subject[ic]:
            current = "M" # := Identity
        elif base == "-":
            current = "D" # := Deletion
        elif subject[ic] == "-":
            current = "I" # := Insertion
        else:
            current = "X" # := Mutation
        ic+=1

        # create the CIGAR string
        if previous == None:
            previous = current
            count += 1
        elif current == previous:
            count += 1
        elif current != previous:
            # write what is given so far
            cigarString += str(count) + previous
            previous = current
            count = 1
        # the last position is reached:
        if ic == len(query):
            cigarString += str(count) + current


# DEBUG
#        print(query[i], subject[i])
#        print(" cur:", current, "prev:", previous, "count:", count)
#        print(" cig:", cigarString)

    return cigarString


# object to store the data obtainded from the blastn search
class blastn_match:
        def __init__(self, qseqid=None, qlen=None, length=None, qstart=None, qend=None, sstart=None, send=None,\
                     sstrand=None, qseq=None, sseq=None, evalue=None, pident=None, nident=None, mismatch=None):
            self.qseqid = qseqid
            self.qlen = qlen
            self.length = length
            self.qstart = qstart
            self.qend = qend
            self.sstart = sstart
            self.send = send
            self.sstrand =sstrand
            self.qseq = qseq
            self.sseq = sseq
            self.evalue = evalue
            self.pident = pident
            self.nident = nident
            self.mismatch = mismatch

# function to create an object
def createMatch(blastn_result):
    lineArray = blastn_result.rstrip().split("\t")
    match = blastn_match( qseqid = lineArray[0],\
                          qlen = int( lineArray[1] ),\
                          length = int( lineArray[2] ),\
                          qstart = int( lineArray[3] ),\
                          qend = int( lineArray[4] ),\
                          sstart = int( lineArray[5] ),\
                          send = int( lineArray[6] ),\
                          sstrand = lineArray[7],\
                          qseq = lineArray[8],\
                          sseq = lineArray[9],\
                          evalue = lineArray[10],\
                          pident = lineArray[11],\
                          nident = lineArray[12],\
                          mismatch = int( lineArray[13] )\
                        )
    return match

# function to create a multimap, containing all ligation sites
# and to collect the related reads within a list
ligationSiteMap = defaultdict(list)
def addLigationSite(ligationSite, FastaID):
    # ligationSite is supposed to harbour a tupel
    # ligationSite is a tupel of the following form: ("NNNN", "NNNN", int, int), being:
    #    upstream sequence, downstream sequence, upstream ending, downstream beginning
    ligationSiteMap[ligationSite].append(FastaID)

# create a dictonary for all blastn results of the first hits
# use ID as key, rest is stored within an object (used as data structure)
firstHitsDict = dict()
for line in file_first_matches:
    match = createMatch(line)
    firstHitsDict[match.qseqid] = match

# create dictonary of all reads
# FastaID used as key
readsDict = dict()
for line in file_reads:
    # FastaID as key = next line is the sequence
    readsDict[line.rstrip()[1:]] = file_reads.readline().rstrip()

# output head of the sam file:
# suitable for Sulfolobus solfataricus
print("@HD\tVN:1.0\tSO:unknown")
print("@SQ\tSN:Chromosome\tLN:2992245")
print("@PG\tID:myOwn\tPN:myOwn\tVN:0.0-beta")

count = 0
for line in file_second_matches:
    secondMatch = createMatch(line)# upstream or downstream is containd within the ID
    position = secondMatch.qseqid.split("_")[1]# upstream or downstream
    originalID = secondMatch.qseqid.split("_")[0]
    firstMatch = firstHitsDict[originalID]

    # DEBUG
    #print("secondMatch:", secondMatch.qseq, secondMatch.sseq, "firstMatch:", firstMatch.qseq, firstMatch.sseq)
    '''
    A SAM file harbors 11 obligatory fields:
    1  QNAME  String  e.g. Read-ID
    2  FLAG   Int     0 if no statement possible
                      16 if mapping to (-)-strand or rev comp
    3  RNAME  String  ReferenceName, here Chromosome
    4  POS    Int     Position, 1-based, left site
    5  MAPQ   Int     mapping quality, 255 if not available
    6  CIGAR  String  statement about the alignment: mutationen and indels
    7  RNEXT  String  name of the next read, * if unknown
    8  PNEXT  Int     position of the next reads, 0 if unkown
    9  TLEN   Int     template LENgth, 0 if unkown
    10 SEQ    String  SEQuence, in the original wihtout gap in case of deletion and alike ...
    11 QUAL   String  QUALity score for every base in the read, e.g. always I if unkown
    '''
    # in case second fragment has been downstream fragment
    if position == "downstream":
        # write downstream fragment first
        rawSequence = secondMatch.qseq.replace("-", "") # that no deletion (-) is create within the sequence
        print(secondMatch.qseqid, "0", "Chromosome", secondMatch.sstart, "255", createCigarString(secondMatch.qseq, secondMatch.sseq), "*", "0", "0", rawSequence, "I" * len(rawSequence), sep="\t") 
        # now write upstream fragment
        rawSequence = firstMatch.qseq.replace("-", "") # that no deletion (-) is create within the sequence
        print(originalID + "_upstream_fragment", "0", "Chromosome", firstMatch.sstart, "255", createCigarString(firstMatch.qseq, firstMatch.sseq), "*", "0", "0", rawSequence, "I" * len(rawSequence), sep="\t")

    # in case second fragment has been upstream fragment
    if position == "upstream":
        # write downstream fragment first
        rawSequence = firstMatch.qseq.replace("-", "") # that no deletion (-) is create within the sequence
        print(originalID + "_downstream_fragment", "0", "Chromosome", firstMatch.sstart, "255", createCigarString(firstMatch.qseq, firstMatch.sseq), "*", "0", "0", rawSequence, "I" * len(rawSequence), sep="\t")
        # now write upstream fragment
        rawSequence = secondMatch.qseq.replace("-", "") # that no deletion (-) is create within the sequence
        print(secondMatch.qseqid, "0", "Chromosome", secondMatch.sstart, "255", createCigarString(secondMatch.qseq, secondMatch.sseq), "*", "0", "0", rawSequence, "I" * len(rawSequence), sep="\t")

# DEBUG
#    count += 1
#    if count >= 100:
#        break
