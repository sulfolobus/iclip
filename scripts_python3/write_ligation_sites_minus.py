#!/usr/bin/env python3

# Usage: ./write_ligation_sites.py 2nd_blastn_minus_strand_uniq.tsv 1st_blastn_minus_overhang.tsv BC1_collapsed_min_32.fasta

import sys
from collections import defaultdict

# Have enough aruments been passed?
if len(sys.argv) != 4:
    print("Wrong usage!\n/write_ligation_sites.py 2nd_blastn_minus_strand_uniq.tsv 1st_blastn_minus_overhang.tsv BC1_collapsed_min_32.fasta\nExit.")
    sys.exit()

second_matches = sys.argv[1]
file_second_matches = open(second_matches, "r")
first_matches = sys.argv[2]
file_first_matches = open(first_matches, "r")
reads = sys.argv[3]
file_reads = open(reads, "r")

# object to store the results of the blastn search
class blastn_match:
        def __init__(self, qseqid=None, qlen=None, length=None, qstart=None, qend=None, sstart=None, send=None,\
                     sstrand=None, qseq=None, sseq=None, evalue=None, pident=None, nident=None, mismatch=None):
            self.qseqid = qseqid
            self.qlen = qlen
            self.length = length
            self.qstart = qstart
            self.qend = qend
            self.sstart = sstart
            self.send = send
            self.sstrand =sstrand
            self.qseq = qseq
            self.sseq = sseq
            self.evalue = evalue
            self.pident = pident
            self.nident = nident
            self.mismatch = mismatch

# function to create an object used as data storage
def createMatch(blastn_result):
    lineArray = blastn_result.rstrip().split("\t")
    match = blastn_match( qseqid = lineArray[0],\
                          qlen = int( lineArray[1] ),\
                          length = int( lineArray[2] ),\
                          qstart = int( lineArray[3] ),\
                          qend = int( lineArray[4] ),\
                          sstart = int( lineArray[5] ),\
                          send = int( lineArray[6] ),\
                          sstrand = lineArray[7],\
                          qseq = lineArray[8],\
                          sseq = lineArray[9],\
                          evalue = lineArray[10],\
                          pident = lineArray[11],\
                          nident = lineArray[12],\
                          mismatch = int( lineArray[13] )\
                        )
    return match

# function to create a multimap, containing all ligation sites
# and save the matching reads in a list
ligationSiteMap = defaultdict(list)
def addLigationSite(ligationSite, FastaID):
    # ligationSite is later suppesed to harbor a tupel
    # with the following form: ("NNNN", "NNNN", int, int), wiht upstream-sequence, downstream-sequence, upstream-ending, downstream-beginning
    ligationSiteMap[ligationSite].append(FastaID)

# create a dictonary for all blastn results of the first hits
# ID = key, rest will be stored within an object (used as data structre)
firstHitsDict = dict()
for line in file_first_matches:
    match = createMatch(line)
    firstHitsDict[match.qseqid] = match

# create dictonary of all reads
# fastaID = key
readsDict = dict()
for line in file_reads:
    # FastaID as key = next line is sequence
    readsDict[line.rstrip()[1:]] = file_reads.readline().rstrip()

# iterate over all second matches:
for line in file_second_matches:
    secondMatch = createMatch(line)# upstream or downstream is to be found with in the ID
    position = secondMatch.qseqid.split("_")[1]# upstream or downstream
    originalID = secondMatch.qseqid.split("_")[0]
    firstMatch = firstHitsDict[originalID]

    # output with the original sequence and where the respective matches align
    print(">", originalID, sep="")
    print(readsDict[originalID])
    print(" " * (firstMatch.qstart - 1), firstMatch.qseq, sep="")
    #print("second hit:", position)
    if position == "downstream":
        print(" " * (firstMatch.qstart -1), " " * len(firstMatch.qseq), secondMatch.qseq, sep="")
    else:
        print(secondMatch.qseq)

    # readout of the ligation sites each 12 nt upstream and downstream
    if position == "downstream":
        print(firstMatch.qseq[-12:], secondMatch.qseq[:12], "Ende upstream-Fragment:", firstMatch.send, "Anfang downstream-Fragment:", secondMatch.sstart)
        # for a short output: without sequence, only the position as separation criteria
        addLigationSite( (firstMatch.send, secondMatch.sstart), originalID)
    else:
        print(secondMatch.qseq[-12:], firstMatch.qseq[:12], "Ende upstream-Fragment:", secondMatch.send, "Anfang downstream-Fragment:", firstMatch.sstart)
        # for a short output: without sequence, only the position as separation criteria
        addLigationSite( (secondMatch.send, firstMatch.sstart), originalID)

    print()# new line at the end


# output of the ligation site
print("End position of upstream fragments", "Start position of downstream fragments", "Length Start -> End", "Amount of reads", "reads", sep="\t", file=sys.stderr)
for key in ligationSiteMap:
    upstreamEnd = key[0]
    downstreamStart = key[1]
    laenge = key[1] - key[0] + 1 # +1, as beginning and end shall be counted as well
    anzahl = len(ligationSiteMap[key])
    # only segments with a minimum length
    if laenge > 20:
        # at least two reads shall be available at thies position
        if anzahl > 1:
            # split the output:
            print(upstreamEnd, downstreamStart, laenge, anzahl, ligationSiteMap[key], sep="\t", file=sys.stderr)
