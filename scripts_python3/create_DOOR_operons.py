#!/usr/bin/env python3

# usage:
# ./create_DOOR_operons.py <input data in tsv format>

import sys

input_tsv = sys.argv[1]

operons = dict()

with open(input_tsv) as f:
    for line in f:
        line = line.split('\t')
        operonID = line[0]
        geneStart = line[3]
        geneEnd = line[4]
        strand = line[5]

        if operonID not in operons:
            operons[operonID] = [geneStart, geneEnd, strand]
        else:
            # update the end position
            operons[operonID][1] = geneEnd


# write the operons
# the lines need to have a specific format:
# Chromosome    me  operon  start   end .   strand  .   ID=operon_XY
for key in operons:
    start = operons[key][0]
    end = operons[key][1]
    strand = operons[key][2]

    print("Chromosome", "me", "operon", start, end, ".", strand, ".", "ID=operon_" + key, sep="\t")
