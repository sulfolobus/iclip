Consider
========

The provided sequences and data from this directory are not part of the source code. They are only provided for convenience and reproducibility of the data analysis. MIT licensing of the code DOES NOT apply to the data provided in this directory. Data were obtained from:

* https://www.ensembl.org
* https://www.ncbi.nlm.nih.gov/refseq/
* http://csbl.bmb.uga.edu/DOOR/
