#!/bin/bash

# Usage:
# ./analyze_3P_overhangs.sh unmapped_reads.fasta

###################################################
: '
Program versions used during analysis:
 Nucleotide-Nucleotide BLAST 2.2.31+
 GNU Awk 4.1.3, API: 1.1
 Python 3.5.2 :: Continuum Analytics, Inc.
 split (GNU coreutils) 8.25
 R scripting front-end version 3.4.2 (2017-09-28)
 GNU bash, version 4.3.48(1)-release
'
###################################################

# directory containing Python3 scripts
py_dir="./scripts_python3/"
# directory containing R scripts
R_dir="./scripts_R/"
# directory containing sequences and annotations
seq_dir="./sequence_dir/"

# input fasta files
in_fasta=${@:2}
in_dir=`dirname $2`

# command line argument for the general results directory
results_dir=$1

# create output directory
output_dir=${results_dir}

if [[ -d $output_dir ]]
then
	rm -irf $output_dir
fi
mkdir $output_dir

###################################################
# blastn search with the unmapped reads
# The 1st blastn search has to be performed with the
# low sequence complexity filter turned off (-dust no),
# as the S. solfataricus genome contains poly-A stretches
# as long as 21 bp. If such a poly-A stretch is cointainded
# within a continuous read we still want to detect it as
# a mappable read.

echo "Performing blastn search for unmapped reads."

current_dir=$output_dir/01_blastn
mkdir $current_dir

# create blast database
blast_dir=$output_dir/blastnDB
mkdir $blast_dir
cp ${seq_dir}/Sulfolobus_solfataricus_p2.ASM700v1.dna.toplevel.fa $blast_dir/
makeblastdb -in $blast_dir/Sulfolobus_solfataricus_p2.ASM700v1.dna.toplevel.fa -dbtype nucl
blastDB=$blast_dir/Sulfolobus_solfataricus_p2.ASM700v1.dna.toplevel.fa

# perform blastn search
for file in $in_fasta
do
	blastn -db $blastDB -query $file -outfmt '6 qseqid qlen qend length sstart send qseq eevalue bitscore mismatch sstrand' -word_size 16 -max_hsps 1 -dust no > $current_dir/`basename ${file%fasta}`tsv
done

last_dir=$current_dir

###################################################
# Only keep resutls with 3'-overhangs

echo "Filtering for 3' overhhangs."

current_dir=$output_dir/02_3P_overhangs
mkdir $current_dir

for file in $last_dir/*tsv
do
	echo $file
	awk '{if( $2 > $3 ){print}}' $file > $current_dir/`basename $file`
done 
last_dir=$current_dir

###################################################
# Write all sequences of 3'-overhangs in to a
# fasta file

echo "Create fasta sequence file for 3'-overhangs."

current_dir=$output_dir/03_fasta_sequences
mkdir $current_dir

for file in $last_dir/*tsv
do
	${py_dir}/extract_3_overhangs.py $in_dir/`basename ${file%tsv}`fasta $file > $current_dir/`basename ${file%tsv}`fasta
done

last_dir=$current_dir

###################################################
# Remove 3'-overhangs that are to short to be
# reasonably processed
# 5 bp is supposed to be the minimum length to be
# used in a subsequent blastn search

echo "Removing very short 3'-overhangs."

current_dir=$output_dir/04_short_sequences
mkdir $current_dir

for file in $last_dir/*fasta
do
	awk '!/^>/ {next} { getline seq } length(seq) >= 5 {print $0 "\n" seq }' $file > $current_dir/`basename $file`
done

last_dir=$current_dir

###################################################
# The now given set of 3'-overhangs has to be
# further processed to remove those 3'-overhangs
# that originate from:
# - circular RNAs
# - potential template switching during PCR
# Computation especially of the many short 3'-over-
# hangs is quite memory demanding. 
###################################################

###################################################
# Remove LONG 3'-overhangs, which map against the
# genomic sequence
# Here we don't want to be to liberal in detecting
# sequences, as the longest poly-A stretch of Sulfolobus
# is 21 As long. Poly-A stretches from the genome should
# already have been found in the first blastn search, where
# the low sequence complexity filter had been disabled (-dust no).
# The choosen settings are therefore:
#  without -dust no
#  -word_size 14 (4**14 = 268.435.456 => statstically uniq in S. solfatracius with 3 Mio bp)
#  -ungapped
#  -perc_identity 100

echo "Remove longer 3'-overhangs mapping to the genome."

current_dir=$output_dir/05_filter_long_overhangs
mkdir $current_dir

# 2nd blastn search to find long 3'-overhangs mapping to the genome
for file in $last_dir/*fasta
do
	blastn -db $blastDB -query $file -outfmt '6 qseqid qlend qend length qseq bitscore mismatch sstrand' -word_size 14 -ungapped -perc_identity 100 -max_hsps 1 > $current_dir/`basename ${file%fasta}`tsv
done

# remove the previously found mapping 3'-overhangs from the candidate list
# required arguments:
# 1. all 3'-fragments -> fasta file
# 2. all 5'-fragments with 3'-overhang -> tsv file
# 3. all 3'-overhangs with blast hit (2nd blastn search) -> tsv file
# 4. output 3'-fragments without blastn hit
# 5. output 5'-fragments without blastn hit
for file in $current_dir/*tsv
do
	${py_dir}/filter_3_overhangs.py \
	$output_dir/04_short_sequences/`basename ${file%tsv}`fasta \
	$output_dir/02_3P_overhangs/`basename $file` \
	$file \
	$current_dir/`basename ${file%.tsv}`_filtered_3_overhangs.fasta \
	$current_dir/`basename ${file%.tsv}`_filtered_5_overhangs.tsv
done

last_results=$current_dir/*_filtered_3_overhangs.fasta

###################################################
# Remove SHORTER 3'-overhangs, which map against
# the genomic sequence 
# CAUTION!
# high memory usage due to blast of short sequences
# minimum 16 GB

echo "Remove shorter 3'-overhangs mapping to the genome."

current_dir=$output_dir/06_filter_short_overhangs
mkdir $current_dir

# perform the 3rd blastn search with
#for file in $last_results
#do
#	blastn -db $blastDB -query $file -outfmt '6 qseqid qlend qend length qseq sstart send bitscore mismatch sstrand' -word_size 5 -evalue 10000 -ungapped -perc_identity 100 -qcov_hsp_perc 100 -dust no > $current_dir/`basename ${file%fasta}`tsv
#done

echo "Parallelized blastn search"

# parallized version of the above blast search
for file in $last_results
do
	# split the fasta file in 20 small ones
	lineNumber=`wc -l $file`
	lineNumber=${lineNumber%% *}
	# we need an even line number for splitting fasta sequences => * 2
	linesPerFile=$(( (lineNumber / 40) * 2 ))
	tmpDir=$current_dir/tmp-`basename ${file}`
	mkdir $tmpDir
	split -l $linesPerFile $file $tmpDir/
	for f in $tmpDir/*
	do
		blastn -db $blastDB -query $f \
		-outfmt '6 qseqid qlend qend length qseq sstart send bitscore mismatch sstrand' \
		-word_size 5 -evalue 10000 -ungapped -perc_identity 100 -qcov_hsp_perc 100 -dust no \
		> $tmpDir/result-`basename $f` &
	done
	wait # wait for the loop to finish
	# combine the single results
	cat $tmpDir/result-* > $current_dir/`basename ${file%fasta}`tsv
	rm $tmpDir/*
	rmdir $tmpDir
	for n in $tmpDir/result-*
	do
		echo $n
	done
	
done

last_dir=$current_dir

###################################################
# Use the blastn results to remove potential
# circulrar RNAs

echo "Removing potential circurlar RNAs from the data set."

current_dir=$output_dir/07_remove_cRNAs
mkdir $current_dir

for file in $last_dir/*tsv
do
	# argument list
	# 1. filtered 3' fragments -> fasta
	# 2. filtered 5' fragments -> tsv
	# 3. 3rd blastn results -> tsv
	${py_dir}/exclude_circRNA_candidates.py \
	$output_dir/05_filter_long_overhangs/`basename ${file%tsv}`fasta \
	$output_dir/05_filter_long_overhangs/`basename ${file%3_overhangs.tsv}`5_overhangs.tsv \
	$file \
	> $current_dir/`basename ${file%_filtered_3_overhangs.tsv}`_3P_attachment.fasta
	# for instance:
	# 05_filter_long_overhangs/BC1_unaligned_filtered_3_overhangs.fasta
	# 05_filter_long_overhangs/BC1_unaligned_filtered_5_overhangs.tsv
	# 06_filter_short_overhangs/BC1_unaligned_filtered_3_overhangs.tsv
done

last_dir=$current_dir

###################################################
# evaluate the results

echo "Performing evaluation of 3' attachments."

current_dir=$output_dir/08_evaluation
mkdir $current_dir

for file in $last_dir/*fasta
do
	./accessory_bin/faCount $file | tail -1 | awk \
'BEGIN {
        countA=0
        countC=0
        countG=0
        countT=0
        countN=0
        laenge=0
}
{
        laenge+=$2
        countA+=$3
        countC+=$4
        countG+=$5
        countT+=$6
        countN+=$7
}
END {
        prozentA = countA * 100 / laenge
        prozentC = countC * 100 / laenge
        prozentG = countG * 100 / laenge
        prozentT = countT * 100 / laenge
        prozentN = countN * 100 / laenge
        print "Number of nucleotides: " laenge
        print "A: " countA " = " prozentA " %"
        print "C: " countC " = " prozentC " %"
        print "G: " countG " = " prozentG " %"
        print "T: " countT " = " prozentT " %"
        print "N: " countN " = " prozentN " %"
        print "GC content: " countC + countG " = " prozentC + prozentG " %"
        print "Sum: " countA + countC + countG + countT + countN " = " prozentA + prozentC + prozentG + prozentT + prozentN " %"
}' > $current_dir/summary_`basename ${file%fasta}`txt
done

for file in $last_dir/*fasta
do
	${py_dir}/perBaseSequenceContent.py $file > $current_dir/statistics_`basename ${file%fasta}`txt
done

# Generate a plot
for file in $current_dir/stat*
do
	Rscript --vanilla ${R_dir}/create_graphic.R $file
done

last_dir=$current_dir


###################################################
# controll for the results:
# We need to see whether the nucleotide content of the
# 3' attachments is special or normal. Therefore we also
# analyse the nucleotide content of the 5' part of the
# respective reads (and only the reads that possess a
# 3' attachment).

echo "Evaluation the nucleotide composition of the 5' part of the reads."

current_dir=$output_dir/09_nucleotide_content_controll
mkdir $current_dir

# a list of the reads with 3' attachments is required
# the respective reads are found in the directory:
#	07_remove_cRNAs
# the fasta IDs can be extracted from those files

for f in $output_dir/07_remove_cRNAs/*fasta
do
	grep ">" $f | tr -d ">" > $current_dir/`basename ${f%fasta}txt`
done

# the corresponding 5' side of the reads is found in the directory:
#	05_filter_long_overhangs
# we use it to create a fasta file with the 5' sides
for f in $current_dir/BC*.txt
do
	# user blocks to parallelize the commands
	{
		currentFile=${f##*/}
		BCnumber=${currentFile%%_*}

		while read line
		do
			grep $line $output_dir/05_filter_long_overhangs/${BCnumber}_unaligned_filtered_5_overhangs.tsv | cut -f 1,7
		done < $f > $current_dir/${BCnumber}_5P_side.txt

		sed 's/^/>/; s/\t/\n/' $current_dir/${BCnumber}_5P_side.txt > $current_dir/${BCnumber}_5P_side.fasta
	}&
done
wait

# now that we have the fasta files of the 5' sides we can
# analyse the nucleotide composition:

for file in $current_dir/*_5P_side.fasta
do
	./accessory_bin/faCount $file | tail -1 | awk \
'BEGIN {
        countA=0
        countC=0
        countG=0
        countT=0
        countN=0
        laenge=0
}
{
        laenge+=$2
        countA+=$3
        countC+=$4
        countG+=$5
        countT+=$6
        countN+=$7
}
END {
        prozentA = countA * 100 / laenge
        prozentC = countC * 100 / laenge
        prozentG = countG * 100 / laenge
        prozentT = countT * 100 / laenge
        prozentN = countN * 100 / laenge
        print "Number of nucleotides: " laenge
        print "A: " countA " = " prozentA " %"
        print "C: " countC " = " prozentC " %"
        print "G: " countG " = " prozentG " %"
        print "T: " countT " = " prozentT " %"
        print "N: " countN " = " prozentN " %"
        print "GC content: " countC + countG " = " prozentC + prozentG " %"
        print "Sum: " countA + countC + countG + countT + countN " = " prozentA + prozentC + prozentG + prozentT + prozentN " %"
}' > $current_dir/`basename ${file%.fasta}`_result.txt
done
