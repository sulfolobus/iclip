#!/bin/bash

# two command line arguments are required:
in_fastq=$2
output_dir=$1 # white space characters will cause problems
# e.g.
# ./read_processing.sh ../unzipped\ Data\ and\ fastqc/Exo_S1_L001_R1_001.fastq testlauf_full

###################################################
: '
Program versions used during analysis:
 FASTX Toolkit 0.0.14
 GNU Awk 4.1.3, API: 1.1
 sed (GNU sed) 4.2.2
 bowtie2 & bowtie2-build 2.3.4
 samtools 1.7, Using htslib 1.7
 bedtools v2.27.1
 GNU bash, version 4.3.48(1)-release
'
###################################################

# directory containing sequences and annotations
seq_dir="./sequence_dir/"

# create output_dir if non existend
if [[ -d $output_dir ]]
then
	echo "Directory $output_dir already exists."
	echo "Exit!"
	exit
else
	mkdir $output_dir
fi

# name of the fastq file to process
fastq_name=`basename "$in_fastq"`

###################################################
# removal of first nucleotide due to bad sequencing
# quality - mostly N
# result will be fastq

echo "Going to remove first base due to bad sequenzing quality."

current_dir=$output_dir/01_1st_bp_trimmmed
output_file=$current_dir/$fastq_name
mkdir $current_dir

fastx_trimmer -f 2 -i "$in_fastq" -o $output_file

###################################################
# Quality control
# result will be fastq

echo "Performing quality filtering"

previous_dir=$current_dir
current_dir=$output_dir/02_quality_trimming
mkdir $current_dir
input_file=$output_file
output_file=$current_dir/$fastq_name

# only some bases may be really bad
echo " Performing 1st filtering step"
fastq_quality_filter -q 5 -p 95 -i $input_file -o ${output_file}_1
# in general most bases should be ok
echo " Performing 2nd filtering step"
fastq_quality_filter -q 20 -p 90 -i ${output_file}_1 -o ${output_file}_2

result=$current_dir/${fastq_name}_2

###################################################
# Remove PCR duplicates
# result will be fasta

echo "Removing reads duplicated during PCR."

current_dir=$output_dir/03_PCR_duplicates
mkdir $current_dir
fasta_name=${fastq_name%q}a

fastx_collapser -v -i $result -o $current_dir/${fasta_name}

result=$current_dir/${fasta_name}

###################################################
# Remove 3' adapter sequences
# result will be fasta

echo "Removing 3'adapter sequences"

current_dir=$output_dir/04_adapter_trimming
mkdir $current_dir

fastx_clipper -a TGAGATCGGA -i $result -o $current_dir/${fasta_name}_1

# make sure that adapters are thoroughly trimmed as
# fastx_clipper doesn´t remove short parts of the
# adapter
# if the read was not trimmed (i.e. length == 74 bp)
# remove matching sequences from the end
#
# using cutadapt in the first place had been an
# alternative to manual trimming via awk, e.g.
#   cutadapt -a TGAGATCGGA -m 19 -O 1 -o <output> <input>
awk '{ 
	if( length($0)!=74){print $0}
	if( length($0)==74){
		str=$0
		sub(/TGAGATC$/, "", str)
		print str
	}
}' $current_dir/${fasta_name}_1 > $current_dir/${fasta_name}_2

awk '{ 
        if( length($0)!=74){print $0}
        if( length($0)==74){
                str=$0
                sub(/TGAGAT$/, "", str)
                print str
        }
}' $current_dir/${fasta_name}_2 > $current_dir/${fasta_name}_3

awk '{ 
        if( length($0)!=74){print $0}
        if( length($0)==74){
                str=$0
                sub(/TGAGA$/, "", str)
                print str
        }
}' $current_dir/${fasta_name}_3 > $current_dir/${fasta_name}_4

awk '{ 
        if( length($0)!=74){print $0}
        if( length($0)==74){
                str=$0
                sub(/TGAG$/, "", str)
                print str
        }
}' $current_dir/${fasta_name}_4 > $current_dir/${fasta_name}_5

awk '{ 
        if( length($0)!=74){print $0}
        if( length($0)==74){
                str=$0
                sub(/TGA$/, "", str)
                print str
        }
}' $current_dir/${fasta_name}_5 > $current_dir/${fasta_name}_6

awk '{ 
        if( length($0)!=74){print $0}
        if( length($0)==74){
                str=$0
                sub(/TG$/, "", str)
                print str
        }
}' $current_dir/${fasta_name}_6 > $current_dir/${fasta_name}_7

awk '{ 
        if( length($0)!=74){print $0}
        if( length($0)==74){
                str=$0
                sub(/T$/, "", str)
                print str
        }
}' $current_dir/${fasta_name}_7 > $current_dir/${fasta_name}_8

result=$current_dir/${fasta_name}_8

###################################################
# Remove the first two bases containing the random
# barcode
# result will be fasta

echo "Removing first barcode"

current_dir=$output_dir/05_trim_2_bases
mkdir $current_dir

fastx_trimmer -f 3 -i $result -o $current_dir/$fasta_name

result=$current_dir/$fasta_name

###################################################
# Remove sequences, that are to short to be mapped
# reasonably.
# Genome size of S. solfataricus is approx. 3 mio bp
# 4^11 = 4,194,304
# there are still several bases left for the barcode
# 4 bp experimental barcode
# 2 bp random barcode
# 11 + 4 + 2 = 17 as a minimum length
#
# results will be fasta

echo "Removing short sequences"

current_dir=$output_dir/06_remove_shot_seqs
mkdir $current_dir

#awk '!/^>/ { next } { getline seq } length(seq) >= 18 { print $0 "\n" seq }' \
#	$result > $current_dir/$fasta_name

echo "This step is not needed anymore. Mapped reads will be filtered
according to MAPQ score >= 20. This will remove all reads with
doubtable mapping quality. Directory is only kept for historic
reasons." >> $current_dir/NOTE.txt

###################################################
# Split sequence file according to experimental
# barcodes.
#
# result will be several fasta files

echo "Splitting according to barcode"

current_dir=$output_dir/07_split_barcodes
mkdir $current_dir

# create barcode file first
echo -e 'BC1\tGGTT
BC2\tGGTC
BC3\tGGCA
BC4\tTGGC
BC5\tCGGA
BC6\tCCGG' > $current_dir/barcodes.txt

cat $result | fastx_barcode_splitter.pl --bcfile $current_dir/barcodes.txt --prefix $current_dir/ --bol --exact --suffix .fasta > $current_dir/statistics

result_dir=$output_dir/07_split_barcodes
fasta_names=BC?.fasta

###################################################
# Remove the remaining barcodes, which is:
# 4 bp experimental barcode
# 2 bp random barcode
#
# result will be several fasta files

echo "Removing remaining barcode"

current_dir=$output_dir/08_remove_barcode
mkdir $current_dir

for file in $result_dir/$fasta_names
do
	fastx_trimmer -f 7 -i $file -o $current_dir/${file##*/}
done

result_dir=$output_dir/08_remove_barcode
fasta_names=BC?.fasta

###################################################
# Mapping with bowtie2
#
# sam file will be saved for mapped reads
# fasta file will be saved for unmapped reads

echo "Mapping of reads"

current_dir=$output_dir/09_mapping
mkdir $current_dir

# The sequence of S. solfataricus is needed for the
# mapping process.
Sulfo_seq=${seq_dir}/Sulfolobus_solfataricus_p2.ASM700v1.dna.toplevel.fa
# Create an index for the mapping.
echo " Create index"
mkdir $current_dir/index
bowtie2-build $Sulfo_seq $current_dir/index/Sulfo_index
bowtie2index=$current_dir/index/Sulfo_index

# Map the reads
for file in $result_dir/$fasta_names
do      
	echo " Mapping reads for: $file"
	# map the reads
	# save unaligned reads as fasta
	# write error messages and statistics to separte file
	bowtie2 -p 4 -f --no-unal -x $bowtie2index $file --un $current_dir/`basename ${file%.fasta}`_unaligned.fasta > $current_dir/`basename ${file%fasta}`sam 2> $current_dir/`basename ${file%.fasta}`-statistics.txt
done

mapped_reads=$output_dir/09_mapping/BC?.sam

###################################################
# Split mapped reads according to strandedness
#
# results will be bam

echo "Separating forward and reverse strand"

current_dir=$output_dir/10_split_strands
mkdir $current_dir

for file in $mapped_reads
do
	# save reads mapping to reverse strand
	samtools view -Shb -f 0x10 $file > $current_dir/`basename ${file%%.sam}`_rev.bam
	# save reads mapping to forward strand
	samtools view -Shb -F 0x10 $file > $current_dir/`basename ${file%%.sam}`_for.bam
done

mappings=$output_dir/10_split_strands/BC*.bam

###################################################
# Convert the mapping into different formats
#
# result will be bedgraph

echo "Generating bedgraph files"

current_dir=$output_dir/11_bedgraph
mkdir $current_dir

for file in $mappings
do
	bedtools genomecov -ibam $file -bg > $current_dir/`basename ${file%%bam}`bedgraph
done

# (-)-strand mapping should be directed downwards
for file in $current_dir/BC*rev.bedgraph
do
	# insert "-" after last tab before the number
	sed -i 's/\(.*\t\)\(.*\)/\1-\2/' $file
done

###################################################
# We want to improve the mapping quality, that only
# "uniquely mapped" reads will be displayed. This
# can be done by filtering the MAPQ score.

echo "Filtering the mapping quality (MAPQ >= 20)."

current_dir=$output_dir/12_filtered_mapping
mkdir $current_dir

for file in $mappings
do
	samtools view -q 20 -b -O BAM $file -o $current_dir/`basename $file`
done

# also convert those files to bedgraph for easy viewing in IGV
for file in $current_dir/*bam
do
	bedtools genomecov -ibam $file -bg > $current_dir/`basename ${file%bam}`bedgraph
done

# bedgraphs for the (-)-strand should point downwords
for file in $current_dir/*_rev.bedgraph
do
	awk '{OFS="\t"}{print($1, $2, $3, -$4)}' $file > ${file%.bedgraph}_down.bedgraph
done

filtered_mapping=$current_dir/*bam

###################################################
# We also want to display just the crosslink sites.

echo "Create mapping of the x-link sites."

current_dir=$output_dir/13_x-link_sites
mkdir $current_dir

# we need the mapped reads in sam format
for file in $filtered_mapping
do
	samtools view -h -o $current_dir/`basename ${file%bam}`sam $file
done

# now map the x-link site for the forward strands
for file in $current_dir/*_for.sam
do
	./scripts_x-links/create_wig_forward.sh $file ${file%sam}wig
done

# and the x-link sites for the reverse strands
for file in $current_dir/*_rev.sam
do
	./scripts_x-links/create_wig_reverse.py $file > ${file%sam}wig
done

# also the mapping of the reverse strand should be
# directed to the downside
for file in $current_dir/*_rev.wig
do
	# write header
	head -2 $file > ${file%.wig}_down.wig
	# add - to each hight (second column)
	tail -n +3 $file | sed 's/ \(.*\)/ -\1/' >> ${file%.wig}_down.wig
done


###################################################
# Finally also some normalized tracks.

echo "Normalization of bedgraph and wig files."

current_dir=$output_dir/14_normalization
mkdir $current_dir


# normalization is performed via RPM (reads per million mapped reads)
# number of mapped reads have to be calculated from the mapping statistics
# found in 09_mapping
for sample in BC{1..6}
do
	# calculate the scaling factor for rpm
	current_mapping=$output_dir/09_mapping/${sample}-statistics.txt
	total_reads=`grep reads $current_mapping | cut -d ' ' -f 1`
	unmapped_reads=`grep 'aligned 0 times' $current_mapping | tr -s ' ' | cut -d ' ' -f 2`
	mapped_reads=$((total_reads - unmapped_reads))

	scaling_factor=`echo "$mapped_reads / 1000000" | bc -l`

	# apply the scaling factor to the bedgraph files
	forward=$output_dir/12_filtered_mapping/${sample}_for.bam
	reverse=$output_dir/12_filtered_mapping/${sample}_rev.bam
	bedtools genomecov -scale $scaling_factor -ibam $forward -bg > $current_dir/${sample}_for.bedgraph &
	bedtools genomecov -scale $scaling_factor -ibam $reverse -bg > $current_dir/${sample}_rev.bedgraph

	# bedgraphs for the (-)-strand should point downwords
	awk '{OFS="\t"}{print($1, $2, $3, -$4)}' $current_dir/${sample}_rev.bedgraph > $current_dir/${sample}_rev_down.bedgraph

	# also apply the scaling factor to the wig files
	wig_for=$output_dir/13_x-link_sites/${sample}_for.wig
	wig_rev=$output_dir/13_x-link_sites/${sample}_for.wig

	head -2 $wig_for > $current_dir/${sample}_for.wig
	awk -v factor=$scaling_factor '{print $1, $2 * factor}' <(tail -n +3 $wig_for) >> $current_dir/${sample}_for.wig

	head -2 $wig_rev > $current_dir/${sample}_rev.wig
	awk -v factor=$scaling_factor '{print $1, $2 * factor}' <(tail -n +3 $wig_rev) >> $current_dir/${sample}_rev.wig
done
