#!/bin/bash

# The purpose of this script is the detection of putative circular RNAs
# in short read mapping data for S. solfataricus.

# USage:
# ./circRNA_detection.sh results_directory unmapped_reads.fasta

###################################################
: '
Program versions used during analysis:
 GNU Awk 4.1.3, API: 1.1
 FASTX Toolkit 0.0.14
 Nucleotide-Nucleotide BLAST 2.2.31+
 Python 3.5.2 :: Continuum Analytics, Inc.
 grep (GNU grep) 2.25
 samtools 1.7, Using htslib 1.7
 bedtools v2.27.1
 GNU bash, version 4.3.48(1)-release
'
###################################################

# directory containing Python3 scripts
py_dir="./scripts_python3/"
# directory containing sequences and annotations
seq_dir="./sequence_dir/"

# input fasta files
in_fasta=${@:2}

# command line argument for the general results directory
results_dir=$1

# create output directory
output_dir=${results_dir}

if [[ -d $output_dir ]]
then
	rm -irf $output_dir
fi
mkdir $output_dir

###################################################
# Filtering of short sequences

echo "Removing short sequences."

current_dir=$output_dir/01_length_filtering
mkdir $current_dir

# only keep reads with 32 bp and longer
# - 20 bp for first blast search
# - 12 bp for second blast search
for file in $in_fasta
do
	awk 'BEGIN{RS=">"; ORS=""} length($2) >= 32 {print ">"$0}' $file > $current_dir/`basename $file`
done

# optionally collapse sequence duplicats
for file in $current_dir/*fasta
do
	fastx_collapser -i $file -o $current_dir/`basename ${file%.fasta}`_collapsed.fasta
done

last_dir=$current_dir

###################################################
# Blast search to find first hits

echo "Performing 1st blastn search."

current_dir=$output_dir/02_1st_blastn
mkdir $current_dir

# create blast database
blast_dir=$output_dir/blastnDB
mkdir $blast_dir
cp ${seq_dir}/Sulfolobus_solfataricus_p2.ASM700v1.dna.toplevel.fa $blast_dir/
makeblastdb -in $blast_dir/Sulfolobus_solfataricus_p2.ASM700v1.dna.toplevel.fa -dbtype nucl
blastDB=$blast_dir/Sulfolobus_solfataricus_p2.ASM700v1.dna.toplevel.fa

# perform blastn search
for file in $last_dir/*fasta
do
	blastn -db $blastDB -query $file -word_size 20 -outfmt '6 qseqid qlen length qstart qend sstart send sstrand qseq sseq evalue pident nident mismatch' -max_hsps 1 > $current_dir/`basename ${file%fasta}`tsv
done

last_dir=$current_dir

######
# DEBUG
#fi
#last_dir=$output_dir/02_1st_blastn
#blastDB=$blast_dir/Sulfolobus_solfataricus_p2.ASM700v1.dna.toplevel.fa


###################################################
# Processing of blast results

echo "Processing of blast results."

current_dir=$output_dir/03_process_1st_blast
mkdir $current_dir

# 1st step - split - and + hits
echo " Split (+)- and (-)-strand hits."
mkdir $current_dir/1_split_strands

for file in $last_dir/*.tsv
do
	grep plus $file > $current_dir/1_split_strands/`basename ${file%.tsv}`_plus.tsv
	grep minus $file > $current_dir/1_split_strands/`basename ${file%.tsv}`_minus.tsv 
done

# 2nd step - keep read with 12 bp overhangs
echo " Keeping only reads with >= 12 bp overhangs."
mkdir $current_dir/2_min_12bp_overhang

for file in $current_dir/1_split_strands/*tsv
do
	awk '{ if( $4 > 12 || ($2 - $5) >= 12 ){ print } }' $file > $current_dir/2_min_12bp_overhang/`basename $file`
done

# 3rd step - extract overhangs
echo " Filtering unaligned overhangs."
mkdir $current_dir/3_extract_overhangs

for file in $current_dir/2_min_12bp_overhang/*.tsv
do
	base=`basename ${file%_*tsv}`
	echo " Processing file: $file"
	${py_dir}/filter_2nd_fragment.py $file $output_dir/01_length_filtering/${base}.fasta > $current_dir/3_extract_overhangs/`basename ${file%.tsv}`.fasta
#	${py_dir}/filter_2nd_fragment.py $file $output_dir/01_length_filtering/${base}.fasta > $current_dir/3_extract_overhangs/${base}_overhang.fasta
done

last_dir=$current_dir/3_extract_overhangs/

###################################################
# 2nd blast search -> 2nd hits

echo "Performing 2nd blastn search."

current_dir=$output_dir/04_2nd_blastn
mkdir $current_dir

# blast for (-)-strand results
for file in $last_dir/*minus.fasta
do
	blastn -db $blastDB -query $file -word_size 12 -outfmt '6 qseqid qlen length qstart qend sstart send sstrand qseq sseq evalue pident nident mismatch' -strand minus > $current_dir/`basename ${file%fasta}tsv`
done

# blast for (+)-strand results
for file in $last_dir/*_plus.fasta
do
	blastn -db $blastDB -query $file -word_size 12 -outfmt '6 qseqid qlen length qstart qend sstart send sstrand qseq sseq evalue pident nident mismatch' -strand plus > $current_dir/`basename ${file%fasta}tsv`
done

###################################################
# Processing 2nd hits

echo "Processing 2nd blastn hits."

current_dir=$output_dir/05_2nd_blastn_processing
mkdir $current_dir

# filter for distance between 1st and 2nd hit
mkdir $current_dir/1_filter_max_distance
for file in $output_dir/03_process_1st_blast/2_min_12bp_overhang/*_minus.tsv
do
	base=`basename ${file}`
#	${py_dir}/select_second_fragments_minusStrand.py $file $output_dir/03_process_1st_blast/3_extract_overhangs/${base} > $current_dir/${base}
	${py_dir}/select_second_fragments_minusStrand.py $file $output_dir/04_2nd_blastn/${base} > $current_dir/1_filter_max_distance/${base}
done

for file in $output_dir/03_process_1st_blast/2_min_12bp_overhang/*_plus.tsv
do
	base=`basename ${file}`
	${py_dir}/select_second_fragments_plusStrand.py $file $output_dir/04_2nd_blastn/${base} > $current_dir/1_filter_max_distance/${base}
done

# filter for ambiguous reads
mkdir $current_dir/2_filter_ambiguous
for file in $current_dir/1_filter_max_distance/*
do
	base=`basename $file`
	${py_dir}/filter_uniq_seqcond_fragments.py $file > $current_dir/2_filter_ambiguous/$base 2> $current_dir/2_filter_ambiguous/${base%.tsv}-ambiguous.tsv
done

last_result_dir=$current_dir/2_filter_ambiguous

###################################################
# Determine the ligation site

echo "Determining the ligation site of putative circRNAs."

current_dir=$output_dir/06_ligation_sites
mkdir $current_dir

# create tables
mkdir $current_dir/1_tables
for file in $last_result_dir/*minus.tsv
do
	base=`basename $file`
	${py_dir}/write_ligation_sites_minus.py $file $output_dir/03_process_1st_blast/2_min_12bp_overhang/$base $output_dir/01_length_filtering/${base%_minus.tsv}.fasta \
	> $current_dir/1_tables/${base%.tsv}_read_list.txt 2> $current_dir/1_tables/${base%.tsv}_tabel.tsv
done

for file in $last_result_dir/*plus.tsv
do
	base=`basename $file`
	${py_dir}/write_ligation_sites_plus.py $file $output_dir/03_process_1st_blast/2_min_12bp_overhang/$base $output_dir/01_length_filtering/${base%_plus.tsv}.fasta \
	> $current_dir/1_tables/${base%.tsv}_read_list.txt 2> $current_dir/1_tables/${base%.tsv}_tabel.tsv
done

# create mappings
mkdir $current_dir/2_mapping_sam
for file in $last_result_dir/*minus.tsv
do
	base=`basename $file`
        ${py_dir}/create_sam_mapping_minus-strand.py $file $output_dir/03_process_1st_blast/2_min_12bp_overhang/$base $output_dir/01_length_filtering/${base%_minus.tsv}.fasta \
	> $current_dir/2_mapping_sam/${base%.tsv}.sam
done

for file in $last_result_dir/*plus.tsv
do
	base=`basename $file`
	${py_dir}/create_sam_mapping_plus-strand.py $file $output_dir/03_process_1st_blast/2_min_12bp_overhang/$base $output_dir/01_length_filtering/${base%_plus.tsv}.fasta \
	> $current_dir/2_mapping_sam/${base%.tsv}.sam
done

# convert sam to sorted bam and index
mkdir $current_dir/3_mapping_bam

for file in $current_dir/2_mapping_sam/*
do
	samtools view -b $file | samtools sort - -O BAM -o $current_dir/3_mapping_bam/`basename ${file%sam}`bam
	samtools index $current_dir/3_mapping_bam/`basename ${file%sam}`bam
done

# convert bam to bedgraph
mkdir $current_dir/4_mapping_bedgraph

for file in $current_dir/3_mapping_bam/*bam
do
	bedtools genomecov -ibam $file -bg > $current_dir/4_mapping_bedgraph/`basename ${file%bam}`bedgraph
done
