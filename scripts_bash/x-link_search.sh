#!/bin/bash

# the purpose of this script is the detection and evaluation of potential
# iCLIP cross-links, that are to be found in the previously mapped reads.
# The input data are composed of the bam files obtained by bowtie2:
# BC1_for.bam	-> Rrp4
# BC1_rev.bam
# BC2_for.bam	-> Rrp41
# BC2_rev.bam
# BC3_for.bam	-> Trx
# BC3_rev.bam
# BC4_for.bam	-> Rrp4
# BC4_rev.bam
# BC5_for.bam	-> Rrp41
# BC5_rev.bam
# BC6_for.bam	-> Trx
# BC6_rev.bam

# Usage:
# ./x-link_search.sh resutls_directory BAM-files

###################################################
: '
Program versions used during analysis:
 samtools 1.7, Using htslib 1.7
 pureclip version: 1.0.4 & SeqAn version: 2.2.0
 Python 3.5.2 :: Continuum Analytics, Inc.
 grep (GNU grep) 2.25
 bedtools v2.27.1
 R version 3.4.2 (2017-09-28) -- "Short Summer"
 sed (GNU sed) 4.2.2
 sort (GNU coreutils) 8.25
 GNU bash, version 4.3.48(1)-release
 meme 4.12.0
'
###################################################

# input bam files
in_bam=${@:2}

# command line argument for the general results directory
results_dir=$1

# create output directory
output_dir=${results_dir}

# directory containing Python3 scripts
py_dir="./scripts_python3/"
# directory containing R scripts
R_dir="./scripts_R/"
# directory containing sequences and annotations
seq_dir="./sequence_dir/"


if [[ -d $output_dir ]]
then
	rm -irf $output_dir
fi
mkdir $output_dir

###################################################
# filtering of mapping quality according to
# MAPQ-score >= 20

echo "Filtering mapping according to MAPQ-score >= 20."

current_dir=$output_dir/01_quality_filtering
mkdir $current_dir

for file in $in_bam
do
	samtools view -q 20 -b -O BAM $file -o $current_dir/`basename $file`
done

result_dir=$current_dir

###################################################
# Merge Replicates, sort them and create an index
# for every mapping file

echo "Merge and sort replicated mappings."

current_dir=$output_dir/02_merge_sort
mkdir $current_dir

samtools merge - $result_dir/BC[14]_for.bam | samtools sort -O BAM -o $current_dir/Rrp4_for.bam
samtools merge - $result_dir/BC[14]_rev.bam | samtools sort -O BAM -o $current_dir/Rrp4_rev.bam
samtools merge - $result_dir/BC[25]_for.bam | samtools sort -O BAM -o $current_dir/Rrp41_for.bam
samtools merge - $result_dir/BC[25]_rev.bam | samtools sort -O BAM -o $current_dir/Rrp41_rev.bam
#samtools merge - $result_dir/BC[36]_for.bam | samtools sort -O BAM -o $current_dir/Trx_for.bam
#samtools merge - $result_dir/BC[36]_rev.bam | samtools sort -O BAM -o $current_dir/Trx_rev.bam
# BC3 and 6 shall not be merged
samtools sort -O BAM -o $current_dir/TrxBC3_for.bam $result_dir/BC3_for.bam
samtools sort -O BAM -o $current_dir/TrxBC3_rev.bam $result_dir/BC3_rev.bam
samtools sort -O BAM -o $current_dir/TrxBC6_for.bam $result_dir/BC6_for.bam
samtools sort -O BAM -o $current_dir/TrxBC6_rev.bam $result_dir/BC6_rev.bam

echo "Create indices."

for file in $current_dir/*bam
do
	samtools index -b $file
done

result_dir=$current_dir

###################################################
# Peak calling with PureCLIP

echo "Cross-link site detection with PureCLIP."

current_dir=$output_dir/03_peak_calling
mkdir $current_dir

for file in $result_dir/*bam
do
	echo -e "\n\tProcessing file: ${file}\n"
	pureclip -i $file -bai $file.bai -g ${seq_dir}/Sulfolobus_solfataricus_p2.ASM700v1.dna.toplevel.fa -nt 4 -o $current_dir/`basename ${file%bam}`_xlinks.bed > /dev/null &
done
wait

result_dir=$current_dir

###################################################
# Distribution of cross-link sites
#
# For the analysis a modified annotation was used.
# Original annotation: GCF_000007005.1_ASM700v1_genomic.gff
# * line 10 containing the full chromosome was removed
# * duplicated positional entries were removed
# * sequence id was changed from NC_002754.1 to Chromosome	

echo "Analysing the distribution of cross-link sites, within the annotated genes."

current_dir=$output_dir/04_peak_distribution_in_genes
mkdir $current_dir

# split every annotation into 10 bins (= deciles)
${py_dir}/dezile.py ${seq_dir}/nurRefSeqChromosome.gff > $current_dir/all_deciles.gff

# create an annotation of its own for every decile
for i in {1..10}
do
	grep Decil_${i}'$' $current_dir/all_deciles.gff >> $current_dir/decile_${i}.gff
done

echo -e "\tCounting cross-links in every decile."

# count the peaks in every decile
for pureclipResults in $result_dir/*.bed
do
	outputFile=`basename ${pureclipResults%_xlinks.bed}`peak-distribution.csv
	for decile in {1..10}
	do
		echo -ne "Decile ${decile}:\t" >> $current_dir/$outputFile
		# performs the actual counting step
		bedtools intersect -s -a $pureclipResults -b $current_dir/decile_${decile}.gff | wc -l >> $current_dir/$outputFile
	done
done

echo -e "\tGenerating images for every cross-link distribution."

# for the graphics to be equally scaled the highest number
# has to be determined
maximum=`cut -f 2 $current_dir/*csv | sort -n | tail -1`

for file in $current_dir/*csv
do
	${R_dir}/graphik.R $file $maximum
done


###################################################
# Assess the cross-link distribution in artificially
# annotated operons

echo "Creating artifically annotated operons."

current_dir=$output_dir/05_peak_distribution_in_artifical_operons
mkdir $current_dir

# Previous approach to define operons:
# cp ${seq_dir}/nurRefSeqChromosome.gff $current_dir/nurRefSeqChromosome.gff
# # merges all features on the same strand, that are not more than
# # 20 nt apart from one another
# bedtools merge -s -d 20 -i $current_dir/nurRefSeqChromosome.gff > $current_dir/RefSeqChromosomeMerged.bed
# # strandedness gets lost during merging
# ${py_dir}/strandedness.py $current_dir > $current_dir/artificial_operons.gff
#
# artificalOperons=$current_dir/artificial_operons.gff

# new approach for operon definition:
# Sulfolobus P2 operons were downloaded from the DOOR database
# http://intelligent6g.com/DOOR3/displayNCoperon.php?id=3025&page=1&nc=NC_002754#tabs-1
# data of accession: 2020, July, 31
# the header of the file has to be removed
tail -n +4 ${seq_dir}/SulfolobusP2_DOOR_database_Operons.opr > $current_dir/SulfolobusP2_DOOR_operons.tsv
# there is an invalid record, that has to be removed:
# 1416730	15896972	SSO12256	2991448	252	+	349	COG1215M	glycosyltransferase
sed -i '/^1416730/d' $current_dir/SulfolobusP2_DOOR_operons.tsv
# convert the format into gff
${py_dir}/create_DOOR_operons.py $current_dir/SulfolobusP2_DOOR_operons.tsv > $current_dir/artificial_operons.gff

artificalOperons=$current_dir/artificial_operons.gff

echo "Assessing distribution of cross-links within the artifical operons."

# same as before

${py_dir}/dezile.py $artificalOperons > $current_dir/all_deciles.gff

for i in {1..10}
do
	grep Decil_${i}'$' $current_dir/all_deciles.gff >> $current_dir/decile_${i}.gff
done

for pureclipResults in $output_dir/03_peak_calling/*.bed
do
	outputFile=`basename ${pureclipResults%_xlinks.bed}`peak-distribution.csv
	for decile in {1..10}
	do
		echo -ne "Decile ${decile}:\t" >> $current_dir/$outputFile
		bedtools intersect -s -a $pureclipResults -b $current_dir/decile_${decile}.gff | wc -l >> $current_dir/$outputFile
	done
done

echo -e "\tGenerating images for every cross-link distribution in operons."

maximum=`cut -f 2 $current_dir/*csv | sort -n | tail -1`
echo $maximum
for file in $current_dir/*csv
do
	echo $file
	${R_dir}/graphik.R $file $maximum
done

###################################################
# The code in the following sections will actually
# repeat for evluation of 3'-UTR and 5'-UTR with
# varying annotations (operons and genes)
###################################################

###################################################
# cross-link distribution around 3'-UTR 
# with artifical operons

echo "Assessing distribution of crosslinks around the 3'-UTR"
echo "within artifical operons and bin size of 10 bp."

current_dir=$output_dir/06_3UTR_artificial_operons_binsize_10
mkdir $current_dir
artificalOperons=$output_dir/05_peak_distribution_in_artifical_operons/artificial_operons.gff

# create bins around the 3'-UTR, 10 bins with 10 nt each
${py_dir}/3UTR_bins.py $artificalOperons 10 10 > $current_dir/bins.gff3
# For the 3'-UTR the bin+1 will be the first bin in the 3'-UTR and
# the bin-1 will be the last bin within the gene.
# To have a closer look a the position of the respective bins, the
# annotated bins can be loaded into a genome browser such as IGV.

for i in {1..10}
do
	grep operon_.*_bin_-${i}$ $current_dir/bins.gff3 >> $current_dir/bins-${i}.gff3
	grep operon_.*_bin_+${i}$ $current_dir/bins.gff3 >> $current_dir/bins+${i}.gff3
done

# this task will be the same in the coming sections
xLinkCounting(){
	# combine all x-links; excluding Trx control
	cat $output_dir/03_peak_calling/R*.bed > $current_dir/x-links-Rrp4_41.bed
	# combine all x-links of Trx
	cat $output_dir/03_peak_calling/Trx*bed > $current_dir/x-links-Trx.bed
	# combine replicated samples
	cat $output_dir/03_peak_calling/Rrp4_*.bed > $current_dir/x-links-Rrp4.bed
	cat $output_dir/03_peak_calling/Rrp41_*.bed > $current_dir/x-links-Rrp41.bed
	# Trx BC3 sample
	cat $output_dir/03_peak_calling/TrxBC3_*.bed > $current_dir/x-links-Trx_BC3.bed

	# count the x-links in the bins
	for bin in $current_dir/bins?*.gff3
	do
	        echo -ne "${bin%.gff3}\t" >> $current_dir/result_Rrp4_41.csv
	        # count x-links of sample
	        bedtools intersect -s -a $bin -b $current_dir/x-links-Rrp4_41.bed | wc -l >> $current_dir/result_Rrp4_41.csv
	        # count x-links of Trx control
	        echo -ne "${bin%.gff3}\t" >> $current_dir/result_Trx.csv
	        bedtools intersect -s -a $bin -b $current_dir/x-links-Trx.bed | wc -l >> $current_dir/result_Trx.csv
	        # count x-links of Rrp4
	        echo -ne "${bin%.gff3}\t" >> $current_dir/result_Rrp4.csv
	        bedtools intersect -s -a $bin -b $current_dir/x-links-Rrp4.bed | wc -l >> $current_dir/result_Rrp4.csv
	        # count x-links of Rrp41
	        echo -ne "${bin%.gff3}\t" >> $current_dir/result_Rrp41.csv
	        bedtools intersect -s -a $bin -b $current_dir/x-links-Rrp41.bed | wc -l >> $current_dir/result_Rrp41.csv
	        # count x-links of Trx BC3
	        echo -ne "${bin%.gff3}\t" >> $current_dir/result_Trx_BC3.csv
	        bedtools intersect -s -a $bin -b $current_dir/x-links-Trx_BC3.bed | wc -l >> $current_dir/result_Trx_BC3.csv
	done
	
	# remove directory prefix
	sed -i 's/.*\///' $current_dir/result_Rrp4_41.csv
	sed -i 's/.*\///' $current_dir/result_Trx.csv
	sed -i 's/.*\///' $current_dir/result_Rrp4.csv
	sed -i 's/.*\///' $current_dir/result_Rrp41.csv
	sed -i 's/.*\///' $current_dir/result_Trx_BC3.csv
	
	
	# sort bins numerically
	sort -k 1.5g $current_dir/result_Rrp4_41.csv > $current_dir/result_Rrp4_41-sort.csv
	sort -k 1.5g $current_dir/result_Trx.csv > $current_dir/result_Trx-sort.csv
	sort -k 1.5g $current_dir/result_Rrp4.csv > $current_dir/result_Rrp4-sort.csv
	sort -k 1.5g $current_dir/result_Rrp41.csv > $current_dir/result_Rrp41-sort.csv
	sort -k 1.5g $current_dir/result_Trx_BC3.csv > $current_dir/result_Trx_BC3-sort.csv
}

xLinkCounting


###################################################
# cross-link distribution around 5'-UTR
# wiht artifical operons

echo "Assessing distribution of crosslinks around the 5'-UTR"
echo "within artifical operons and bin size of 10 bp."

current_dir=$output_dir/07_5UTR_artificial_operons_binsize_10
mkdir $current_dir
# operon annotation as before
artificalOperons=$output_dir/05_peak_distribution_in_artifical_operons/artificial_operons.gff

# create bins around the 5'-UTR, 10 bins with 10 nt each
${py_dir}/5UTR_bins.py $artificalOperons 10 10 > $current_dir/bins.gff3
# For the 5'-UTR the bin+1 will be the first bin in the gene and
# the bin-1 will be the last bin within the 5'-UTR. Or before the
# gene, as many genes are leaderless transcripts.

for i in {1..10}
do
	grep operon_.*_bin_-${i}$ $current_dir/bins.gff3 >> $current_dir/bins-${i}.gff3
	grep operon_.*_bin_+${i}$ $current_dir/bins.gff3 >> $current_dir/bins+${i}.gff3
done

xLinkCounting

###################################################
# cross-link distribution around 3'-UTR 
# with artifical operons
# over 100 nt

echo "Assessing distribution of crosslinks around the 3'-UTR"
echo "within artifical operons and bin size of 1 bp."

current_dir=$output_dir/08_3UTR_artificial_operons_binsize_1
mkdir $current_dir
artificalOperons=$output_dir/05_peak_distribution_in_artifical_operons/artificial_operons.gff

# create bins around the 3'-UTR, 100 bins with 1 nt each
${py_dir}/3UTR_bins.py $artificalOperons 1 100 > $current_dir/bins.gff3

for i in {1..100}
do
	grep operon_.*_bin_-${i}$ $current_dir/bins.gff3 >> $current_dir/bins-${i}.gff3
	grep operon_.*_bin_+${i}$ $current_dir/bins.gff3 >> $current_dir/bins+${i}.gff3
done

xLinkCounting


###################################################
# cross-link distribution around 5'-UTR
# wiht artifical operons
# over 100 nt

echo "Assessing distribution of crosslinks around the 5'-UTR"
echo "within artifical operons and bin size of 1 bp."

current_dir=$output_dir/09_5UTR_artificial_operons_binsize_1
mkdir $current_dir
# operon annotation as before
artificalOperons=$output_dir/05_peak_distribution_in_artifical_operons/artificial_operons.gff

# create bins around the 5'-UTR, 100 bins with 1 nt each
${py_dir}/5UTR_bins.py $artificalOperons 1 100 > $current_dir/bins.gff3

for i in {1..100}
do
	grep operon_.*_bin_-${i}$ $current_dir/bins.gff3 >> $current_dir/bins-${i}.gff3
	grep operon_.*_bin_+${i}$ $current_dir/bins.gff3 >> $current_dir/bins+${i}.gff3
done

xLinkCounting


###################################################
# cross-link distribution around 3'-UTR 
# with real annotation

echo "Assessing distribution of crosslinks around the 3'-UTR"
echo "within real annotation and bin size of 10 bp."

current_dir=$output_dir/10_3UTR_real_annotation_binsize_10
mkdir $current_dir

sed 's/;.*//' ${seq_dir}/nurRefSeqChromosome.gff > $current_dir/annotation.gff
annotation=$current_dir/annotation.gff

# create bins around the 3'-UTR, 10 bins with 10 nt each
${py_dir}/3UTR_bins.py $annotation 10 10 > $current_dir/bins.gff3

for i in {1..10}
do
	grep gene.*_bin_-${i}$ $current_dir/bins.gff3 >> $current_dir/bins-${i}.gff3
	grep gene.*_bin_+${i}$ $current_dir/bins.gff3 >> $current_dir/bins+${i}.gff3
done

xLinkCounting


###################################################
# cross-link distribution around 5'-UTR
# with real annotation

echo "Assessing distribution of crosslinks around the 5'-UTR"
echo "within real annotation and bin size of 10 bp."

current_dir=$output_dir/11_5UTR_real_annotation_binsize_10
mkdir $current_dir

# create bins around the 5'-UTR, 10 bins with 10 nt each
${py_dir}/5UTR_bins.py $annotation 10 10 > $current_dir/bins.gff3

for i in {1..10}
do
	grep gene.*_bin_-${i}$ $current_dir/bins.gff3 >> $current_dir/bins-${i}.gff3
	grep gene.*_bin_+${i}$ $current_dir/bins.gff3 >> $current_dir/bins+${i}.gff3
done

xLinkCounting


###################################################
# cross-link distribution around 3'-UTR 
# with real annotation
# over 100 nt

echo "Assessing distribution of crosslinks around the 3'-UTR"
echo "within real annotation and bin size of 1 bp."

current_dir=$output_dir/12_3UTR_real_annotation_binsize_1
mkdir $current_dir

# create bins around the 3'-UTR, 100 bins with 1 nt each
${py_dir}/3UTR_bins.py $annotation 1 100 > $current_dir/bins.gff3

for i in {1..100}
do
	grep gene.*_bin_-${i}$ $current_dir/bins.gff3 >> $current_dir/bins-${i}.gff3
	grep gene.*_bin_+${i}$ $current_dir/bins.gff3 >> $current_dir/bins+${i}.gff3
done

xLinkCounting


###################################################
# cross-link distribution around 5'-UTR
# with real annotation
# over 100 nt

echo "Assessing distribution of crosslinks around the 5'-UTR"
echo "within real annotation and bin size of 1 bp."

current_dir=$output_dir/13_5UTR_real_annotation_binsize_1
mkdir $current_dir

# create bins around the 5'-UTR, 100 bins with 1 nt each
${py_dir}/5UTR_bins.py $annotation 1 100 > $current_dir/bins.gff3

for i in {1..100}
do
	grep gene.*_bin_-${i}$ $current_dir/bins.gff3 >> $current_dir/bins-${i}.gff3
	grep gene.*_bin_+${i}$ $current_dir/bins.gff3 >> $current_dir/bins+${i}.gff3
done

xLinkCounting


###################################################
# Example Analysis of cross-link distribution
# for annoted genes around the 5'-UTR with a
# given bin size of 1. A similar analysis can
# be performed for the other bin sizes, regions
# and annotations.

echo "Analyzing cross-link distribution around the 5'-UTR."

current_dir=$output_dir/14_example_analysis_5UTR_genes
mkdir $current_dir

cp $output_dir/13_5UTR_real_annotation_binsize_1/result_Rrp*-sort.csv $current_dir/

${R_dir}/5UTR_bin_size_1.R $current_dir/result_Rrp4-sort.csv $current_dir/result_Rrp41-sort.csv


###################################################
# Finally after creating the previous graphic we
# can closely inspect it, to inverstigate if there
# are any interesting positions. With the given
# example we can roughly estimate, that there is a
# peak around the position -30.
# As the exact position of the peak can only be
# roughly estimated from the picture, we need to
# have a closer look at the primary data, in this
# case the data that's been use to generate the
# graphic:
# /13_5UTR_real_annotation_binsize_1/result_Rrp4-sort.csv || result_Rrp41-sort.csv
# This tells us, that the peak is indeed at position
# -30. This information can now be used to extract
# all sequences, that contributed to this peak and
# calculate a kind of consensus sequence using
# meme.

echo -e "Finding consensus sequence for the -30 peak in the cross-link distribution.\n"

current_dir=$output_dir/15_consensus_sequence
mkdir $current_dir

# we need the following data
cp $output_dir/13_5UTR_real_annotation_binsize_1/bins-30.gff3 $current_dir
cp $output_dir/13_5UTR_real_annotation_binsize_1/x-links-Rrp4.bed $current_dir


# determine the exact genomic positions of the x-links
bedtools intersect -s -a $current_dir/bins-30.gff3 -b $current_dir/x-links-Rrp4.bed > $current_dir/x-link_positions.txt

# the exact sequence surrounding those positions can now
# be extracted, adding 10 bp to both sides (21 bp total)
for region in `cut -f 4 $current_dir/x-link_positions.txt`
do
        samtools faidx $seq_dir/Sulfolobus_solfataricus_p2.ASM700v1.dna.toplevel.fa Chromosome:$((region-10))-$((region+10))
done >> $current_dir/sequences.fa

sed -n '1~2!p' $current_dir/sequences.fa > $current_dir/sequences.txt

# some of the x-links origin from the (-)-strand so
# the rev comp is needed for those sequences
cut -f 7 $current_dir/x-link_positions.txt | paste $current_dir/sequences.txt - | while read seq strand
do
	if [[ $strand == "-" ]]
	then
		echo $seq | tr ATGC TACG | rev
	else
		echo $seq
	fi
done >> $current_dir/seq_5_3.txt

# create a fasta file for use in meme
nl $current_dir/seq_5_3.txt | sed -r 's/^\s*//' | while read num seq; do   echo ">$num";   echo "$seq"; done > $current_dir/seqs_5_3.fasta

# now we can a kind of consensus sequence using meme
meme $current_dir/seqs_5_3.fasta -o $current_dir/memeResult -dna -nmotifs 1 -minw 4 -maxw 10
